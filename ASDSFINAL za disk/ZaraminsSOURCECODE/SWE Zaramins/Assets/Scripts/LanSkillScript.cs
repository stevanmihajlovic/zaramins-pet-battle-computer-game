﻿using UnityEngine;
using System.Collections;

public class LanSkillScript : MonoBehaviour
{

    private LanGameControlScript gameController;
    private TextMesh infoText;


    // Use this for initialization
    void Start()
    {
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        gameController = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseUpAsButton()
    {
        if (gameController.phase == 1)
        {
            if (this.name == "FirstAttack")

                gameController.phase12(0);

            else
                if (this.name == "SecondAttack")

                    gameController.phase12(1);

                else

                    gameController.phase12(2);
        }

    }

    void OnMouseOver()
    {
        if (name == "FirstAttack")
        {
            infoText.text = gameController.magicInfo(0);
        }
        else
            if (name == "SecondAttack")
            {
                infoText.text = gameController.magicInfo(1);
            }
            else
            {
                infoText.text = gameController.magicInfo(2);
            }
    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
