﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

public class LogInScript : MonoBehaviour
{
    //Objekat koji sadrzi sve magije (i sve vezano za njih) i mesto za player name
    public PlayerScript player;
    //Koristi se za tabovanje i kraj unosa podataka (enter, return)
    EventSystem system;
    //Sprite za on i off stanje
    public Sprite on,off;
    //Za infobox za greske
    private InfoBoxScript infoBox;
    public SoundManagerScript script;

	void Start ()
	{
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
        system = EventSystem.current;
    }
	
	public void Update()//Kod za tabovanje	
	{		
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			if (next!= null) 
				{			
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(system));
                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
				}
		}

		if (Input.GetKeyDown("enter") || Input.GetKeyDown("return"))
            OnMouseUpAsButton();
	}


    void OnMouseUpAsButton()//Kod za logovanje
    {
        string username;    
        string password;
        username = GameObject.Find("Canvas/Username/Text").GetComponent<Text>().text;
        password = GameObject.Find("Canvas/Password").GetComponent<InputField>().text;
        script.onClickSound();//aleksa dodao jer je noob
        if (PlayerPrefs.HasKey(username) && username != "")
            if (password == PlayerPrefs.GetString(username) && password != "")
            {
                player.PlayerScriptConstructor(username);//Inicijalizacija samog igraca, i svih karakteristika
                Application.LoadLevel(2);
            }
            else
                infoBox.Show("Wrong Password!", "You have entered\nwrong password!");
        else
            infoBox.Show("Username invalid", "You have entered\nnon-existant username!");
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

}
