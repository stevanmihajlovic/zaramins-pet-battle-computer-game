﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;
//using UnityEditor;


public class MyZaraminsGlow : MonoBehaviour
{
    private static GameObject glow;//Samo jedan glow sme biti aktivan
    private MyZaraminsController controller;
    private InfoBoxScript infoBox;
    public SoundManagerScript script;

	void Start ()
    {
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>(); 
        controller = GameObject.Find("MyZaraminsController").GetComponent<MyZaraminsController>();
        glow = GameObject.Find("Zaramins/Zaramin1/SelectedGlow");//Uzme referencu zbog gasenja
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	}

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.RockSound();
        if (GetComponent<SpriteRenderer>().sprite.name != "Question")
        {
            glow.SetActive(false);//Gasi se predjasnji glow
            if (name == "TeamZaramin1" || name == "TeamZaramin2" || name == "TeamZaramin3")//Da li je kliknut timski
            {
                glow = GameObject.Find("Team/" + name + "/SelectedGlow");
                controller.selectZaraminChangeCard(Convert.ToInt32(tag), "team");
            }
            else
            {
                glow = GameObject.Find("Zaramins/" + name + "/SelectedGlow");//Ili solo Zaramin
                controller.selectZaraminChangeCard(Convert.ToInt32(tag), "solo");
            }
            glow.SetActive(true);//Upali glow
        }
        else
        {
            if (GameObject.Find("Player").GetComponent<PlayerScript>().getZaraminsCount() == 10)
                infoBox.Show("Maximum reached!", "You cannot have more than ten (10)\nactive Zaramins!");
            else
            {
                GameObject.Find("AddZaraminPanel").transform.position = new Vector3(0, 0, -3.5f);//Pomeranje panela
                GameObject.Find("Canvas/InputField").transform.localScale = new Vector3(0.9f, 1, 1);//Obnavljanje InputFielda
            }
        }
    }

    public void ChangeGlow()
    {
        glow.SetActive(false);
        glow = GameObject.Find("Zaramins/" + name + "/SelectedGlow");
        glow.SetActive(true);
    }

}
