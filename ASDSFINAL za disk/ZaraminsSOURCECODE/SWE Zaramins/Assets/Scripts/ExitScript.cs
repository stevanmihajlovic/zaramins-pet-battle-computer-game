﻿using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {

    public SoundManagerScript script;
	// Use this for initialization
	void Start () {

        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        Application.Quit();
    }
}
