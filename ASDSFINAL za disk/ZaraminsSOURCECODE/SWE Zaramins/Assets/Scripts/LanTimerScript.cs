﻿using UnityEngine;
using System.Collections;

public class LanTimerScript : MonoBehaviour
{

    public LanGameControlScript control = null;
    public GameObject timeFill;
    public GameObject timeBackground;
    public float length;
    public float duration;
    public float current_timer;
    public Vector3 start_pos;
    float increment;
    public bool running;
    public float time;
    public NetworkView net;



    // Use this for initialization
    void Start()
    {

        timeFill = GameObject.Find("Timer/TimeBarFill");
        timeBackground = GameObject.Find("Timer/TimeBackground");
        length = timeBackground.transform.localScale.x;
        duration = 45f;
        increment = length / duration;
        start_pos = timeFill.transform.localScale;
        running = false;
        control = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
        net = GetComponent<NetworkView>();
    }

    // Update is called once per frame
    void Update()
    {

        if (running)
        {
            time = Time.deltaTime;
            current_timer += time;
            if (current_timer >= duration)
            {
                if (Network.isServer && control.phase < 3)
                {
                    control.net.RPC("setPhase", RPCMode.Others, 0);
                    control.setPhase(4);
                    control.net.RPC("lanUpdateZaramins", RPCMode.Others);
                    control.selfUpdate();
                }
                else
                {
                    control.net.RPC("setPhase", RPCMode.Others, 4);
                    control.setPhase(0);
                    control.lanUpdateZaramins();
                    control.net.RPC("selfUpdate", RPCMode.Others);                    
                }
                net.RPC("ResetTimer", RPCMode.All);
            }
            else
            {
                timeFill.transform.localScale += new Vector3(time * increment, 0, 0);
            }
        }

    }

    [RPC]
    public void StartTimer()
    {
        running = true;
        current_timer = 0f;
        Debug.Log("Start");
    }

    [RPC]
    public void ResetTimer()
    {
        current_timer = 0f;
        timeFill.transform.localScale = start_pos;
        running = true;
    }

    [RPC]
    public void StopTimer()
    {
        this.running = false;
    }
}
