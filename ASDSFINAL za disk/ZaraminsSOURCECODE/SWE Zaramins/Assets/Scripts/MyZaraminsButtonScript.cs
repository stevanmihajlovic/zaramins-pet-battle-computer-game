﻿using UnityEngine;
using System.Collections;

public class MyZaraminsButtonScript : MonoBehaviour
{
    //Sprite za on i off stanje
    public Sprite on, off;
    public SoundManagerScript script;
	// Use this for initialization
	void Start () {

        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        Application.LoadLevel(3);
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(3, 3, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }
}
