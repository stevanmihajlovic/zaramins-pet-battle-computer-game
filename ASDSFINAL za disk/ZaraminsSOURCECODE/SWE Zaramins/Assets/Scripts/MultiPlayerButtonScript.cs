﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEditor;

public class MultiPlayerButtonScript : MonoBehaviour
{
    PlayerScript player;
    //Sprite za on i off stanje
    public Sprite on, off;
    private InfoBoxScript infoBox;
    public SoundManagerScript script;

	void Start ()
    {
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	void Update ()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        if (!player.TeamFull())
            infoBox.Show("Team missing!","You need to have at least\n3 Zaramins in your team\nfor this mode!");
        else
            Application.LoadLevel(6);
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(3, 3, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }
}
