﻿using UnityEngine;
using System.Collections;

public class InfoBoxScript : MonoBehaviour
{
    public TextMesh naslov, tekst;

	
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show(string nas, string txt)
    {
        naslov.text = nas;
        tekst.text = txt;
        transform.position = new Vector3(0, 0, -8);        
    }
}
