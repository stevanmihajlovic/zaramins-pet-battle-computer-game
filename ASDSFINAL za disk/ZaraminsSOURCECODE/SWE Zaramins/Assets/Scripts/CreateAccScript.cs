﻿using UnityEngine;
using System.Collections;

public class CreateAccScript : MonoBehaviour
{
    //Sprite za on i off stanje
    public Sprite on, off;
    public SoundManagerScript script;
	void Start ()//Pozivanje mask animacije 
    {
        GameObject.Find("Mask").GetComponent<Animation>().Play();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	}

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        Application.LoadLevel(1);
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }
}
