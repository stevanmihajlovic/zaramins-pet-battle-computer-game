﻿using UnityEngine;
using System.Collections;

public class BuyPotionsScript : MonoBehaviour
{
    private PlayerScript player;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()//Klik
    {
        if (player.getGold() > 300)
        {
            player.buyPot(name);
            GameObject.Find("Statistics/StatText").GetComponent<TextMesh>().text = player.getPlayerInfo();
        }
        else
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("No 300 gold!", "Play some games\nto earn more gold");
    }
}
