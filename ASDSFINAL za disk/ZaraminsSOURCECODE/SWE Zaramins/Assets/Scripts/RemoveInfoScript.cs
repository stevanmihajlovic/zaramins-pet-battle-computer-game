﻿using UnityEngine;
using System.Collections;

public class RemoveInfoScript : MonoBehaviour
{
    GameObject box;
    public SoundManagerScript script;

	void Start ()
    {
        box = GameObject.Find("InfoBox");
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();

	}

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        box.transform.position = new Vector3(-20, 0, -8);
    }
}
