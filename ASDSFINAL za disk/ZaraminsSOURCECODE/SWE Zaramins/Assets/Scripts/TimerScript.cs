﻿using UnityEngine;
using System.Collections;

public class TimerScript : MonoBehaviour {

    public GameControlScript control = null;
    public GameObject timeFill;
    public GameObject timeBackground;
    public float length;
    public float duration;
    public float current_timer;
    public Vector3 start_pos;
    float increment;
    public bool running;
    public float time;



	// Use this for initialization
	void Start () {

        timeFill = GameObject.Find("Timer/TimeBarFill");
        timeBackground = GameObject.Find("Timer/TimeBackground");
        length = timeBackground.transform.localScale.x;
        duration = 45f;
        increment = length / duration;
        start_pos = timeFill.transform.localScale;
        running = false;
        
	
	}
	
	// Update is called once per frame
	void Update () {

        if (running)
        {
            time = Time.deltaTime;
            current_timer += time;
            if (current_timer>=duration)
            {                           
                control.changePlayer();
                ResetTimer();
                
            }
            else
            {
                timeFill.transform.localScale += new Vector3(time * increment, 0, 0);                
            }
        }
	
	}
    public void StartTimer ()
    {
        running = true;
        current_timer = 0f;
        
    }
    public void ResetTimer ()
    {       
        current_timer = 0f;
        timeFill.transform.localScale = start_pos;
        running = true;
    }
    public void StopTimer ()
    {
        running = false;
    }
}
