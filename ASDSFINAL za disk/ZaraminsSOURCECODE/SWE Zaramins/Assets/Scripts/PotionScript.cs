﻿using UnityEngine;
using System.Collections;

public class PotionScript : MonoBehaviour
{
    public GameControlScript gameController;
    private PlayerScript player;
    private TextMesh infoText;
    public SoundManagerScript script;

	void Start ()
    {
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    void Update()
    {}

    void OnMouseUpAsButton()
    {
        if (gameController.phase == 0 || gameController.phase == 1 || gameController.phase == 2)
        {
            script.onClickSound();
            gameController.phasex2(name);
        }
    }

    void OnMouseOver()
    {
        if (name == "Health")
            infoText.text = "Use this potion to\nrestore 100 health\npoints to friendly\nZaramin";
        else
            infoText.text = "Use this potion to\nrestore 100 mana\npoints to friendly\nZaramin";
    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
