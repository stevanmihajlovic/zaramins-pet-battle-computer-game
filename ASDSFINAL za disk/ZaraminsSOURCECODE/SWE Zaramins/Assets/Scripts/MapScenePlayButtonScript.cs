﻿using UnityEngine;
using System.Collections;

public class MapScenePlayButtonScript : MonoBehaviour {

    public LevelSceneControllerScript skripta;
    public AudioClip prepareForBattle;
    private AudioSource source;
    public SoundManagerScript script;
	// Use this for initialization
	void Start () {

        source = GetComponent<AudioSource>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseUpAsButton ()
    {
        skripta.player.currentLevel = skripta.selectedLevel;
        if (skripta.selectedLevel > 0)
        {
            script.onClickSound();
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("Prepare for battle", "Battle will begin in a moment");
            StartCoroutine(Delay());
            
        }
        else
        {
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("No level selected!", "Please choose some available\nlevel before enter in\n the battle.");
        }
    }
    IEnumerator Delay ()
    {
        script.StopSound();
        script.PrepareBattle();
        yield return new WaitForSeconds(5);
        Application.LoadLevel(5);
    }
}
