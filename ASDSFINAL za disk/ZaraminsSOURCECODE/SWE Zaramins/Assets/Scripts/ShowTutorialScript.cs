﻿using UnityEngine;
using System.Collections;

public class ShowTutorialScript : MonoBehaviour
{
    public GameObject tutorialTextMask;
    public SoundManagerScript script;



	void Start ()
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

	void Update ()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        tutorialTextMask.SetActive(true);
    }
}
