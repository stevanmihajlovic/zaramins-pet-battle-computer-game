﻿using UnityEngine;
using System.Collections;

public class VolumeScript : MonoBehaviour {

    public SoundManagerScript script;
    public Sprite volumeOn, volumeOff;
    private SpriteRenderer icon;
	// Use this for initialization
	void Start () 
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
        icon = GameObject.Find("Music").GetComponent<SpriteRenderer>();
        if (script.source.volume == 1)
            icon.sprite = volumeOn;
        else
            icon.sprite = volumeOff;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseUpAsButton()
    {
        if (script.source.volume == 1)
        {
            script.source.volume = 0;
            icon.sprite = volumeOff;
        }
        else
        {
            script.source.volume = 1;
            icon.sprite = volumeOn;
        }
    }
}
