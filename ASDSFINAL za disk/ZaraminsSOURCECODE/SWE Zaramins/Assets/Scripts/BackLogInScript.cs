﻿using UnityEngine;
using System.Collections;

public class BackLogInScript : MonoBehaviour
{
    public SoundManagerScript script;
	void Start ()
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        Destroy(GameObject.Find("Player"));
        Destroy(GameObject.Find("MagicPanel"));
        Destroy(GameObject.Find("SoundManager"));
        Application.LoadLevel(0);
    }
}
