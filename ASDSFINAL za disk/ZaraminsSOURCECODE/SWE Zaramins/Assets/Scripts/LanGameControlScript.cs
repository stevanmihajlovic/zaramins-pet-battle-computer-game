﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;


public class LanGameControlScript : MonoBehaviour
{
    private int level, zaraminForUpdate, currentMagicIndex;
    public int phase;
    private string opponentName, potion;
    public bool battleRunning = false, gameFinished = false, potionUsed = false;

    public Zaramin firstZaraminClicked, secondZaraminClicked;
    public Magic magicClicked;//---->menja vrednost tokom borbe

    public PlayerScript player;
    public Zaramin[] enemyZaramins;
    private MagicPanelScript magicPanel;

    private GameObject friendlyZaramins, hostileZaramins;//Grupe Zaramina
    public GameObject chat;

    private GameObject playerTimerOpponent, mask, drawer, healthFill, manaFill;

    public LanTimerScript Timer;

    private SpriteRenderer firstMagic, secondMagic, thirdMagic;

    private float length;

    private BackgroundScript background;

    public LANInfoBoxScript infoBox;

    private InfoBoxScript infoBoxx;

    private Animation yourturn, opponentsturn;

    public Text log;

    public NetworkView net;
    public SoundManagerScript script;

    void Start()
    {
        phase = 0;
        firstMagic = GameObject.Find("Drawer/FirstAttack").GetComponent<SpriteRenderer>();
        secondMagic = GameObject.Find("Drawer/SecondAttack").GetComponent<SpriteRenderer>();
        thirdMagic = GameObject.Find("Drawer/ThirdAttack").GetComponent<SpriteRenderer>();

        yourturn = GameObject.Find("YourTurn").GetComponent<Animation>();
        opponentsturn = GameObject.Find("OpponentTurn").GetComponent<Animation>();

        chat.SetActive(false);

        infoBox = GameObject.Find("InfoBoxx").GetComponent<LANInfoBoxScript>();
        infoBoxx = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();

        mask = GameObject.Find("Mask");
        drawer = GameObject.Find("Drawer");
        friendlyZaramins = GameObject.Find("FriendlyZaramins");
        hostileZaramins = GameObject.Find("HostileZaramins");
        healthFill = GameObject.Find("Zaramin1/HPMana/Health/TimeBarFill");
        

        Timer = GameObject.Find("PTO/Timer").GetComponent<LanTimerScript>();
        Timer.control = this;

        magicPanel = GameObject.Find("MagicPanel").GetComponent<MagicPanelScript>();

        player = GameObject.Find("Player").GetComponent<PlayerScript>();

        background = GameObject.Find("Background").GetComponent<BackgroundScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();

        length = healthFill.transform.localScale.x;

        GameObject.Find("PTO/Player/PlayerText").GetComponent<TextMesh>().text = player.getName() + "\nLevel: " + player.getLevel();
        //GameObject.Find("PTO/Opponent/OpponentText").GetComponent<TextMesh>().text = "Boss\nLevel: 9000";

        net = GetComponent<NetworkView>();
        enemyZaramins = new Zaramin[3];

     }
    
    void Update()
    {}
  
    public void phase01(Vector3 vector, int number)//Odaberi Zaramina ILI POTION
    {
        potionUsed = false;
        firstZaraminClicked = player.myZaramins[player.activeZaramins[number]];//Pamtimo prvog kliknutog Zaramina
        zaraminForUpdate = number;

        if (!firstZaraminClicked.isStunned() && !firstZaraminClicked.isDead())
        {
            drawer.transform.position = vector;//Pomeranje drawera

            drawDrawer();//Crtanje magija

            mask.transform.position = new Vector3(0, 0, 0);//Pomeranje maske

            phase = 1;

            zaraminForUpdate = number;//Ciji HP i mana bar menjamo na kraju
        }
        else
            if (firstZaraminClicked.isStunned())
                infoBoxx.Show("Stunned!", "That Zaramin is\nstunned!");
            else
                infoBoxx.Show("Dead!", "That Zaramin is\ndead!");
    }
    
    public void phase10()//Povratak u prethodno stanje
    {
        drawer.transform.position = new Vector3(20, 0);//Sklanjamo drawer

        phase = 0;

        mask.transform.position = new Vector3(20, 0);//Sklanjamo masku
    }
    
    public void phase12(int number)
    {
        magicClicked = firstZaraminClicked.getMagics()[number];//Pamtimo izabranu magiju
        potionUsed = false;
        currentMagicIndex = number;

        //Provera da li je magiju moguce iskoristiti
        if (firstZaraminClicked.getCooldowns()[number] == 0 && firstZaraminClicked.getCurrentMana() >= magicClicked.getMana())
        {
            if (magicClicked.getFriendly())//Da li je magija prijateljska
            {
                if (magicClicked.getSelf())//Da li je self-only
                {
                    if (firstZaraminClicked.getType() != "Healer")
                    {
                        firstZaraminClicked.SetBuffed(2);
                        net.RPC("lanSetBuffed", RPCMode.Others, new object[] { zaraminForUpdate, 2 });
                    }
                    firstZaraminClicked.DoSelf();//Odradi self magiju
                    net.RPC("lanDoSelf", RPCMode.Others, new object[] { zaraminForUpdate });

                    string msg = "\n>>" + firstZaraminClicked.getName() + " used " + magicClicked.getName() + " on itself" +
                    ("Healer" == firstZaraminClicked.getType() ? (". Healed itself for " + -magicClicked.getDamage() + " points!") : "!");
                    //log.text += msg;PROVERITI!
                    net.RPC("sendMsg", RPCMode.All, msg);

                    phase34();//Kraj poteza

                    return;
                }

                friendlyForward();
            }

            else
                hostileForward();//Menjamo mesta Zaraminima

            drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, 2);

            phase = 2;

        }
        else
            if (firstZaraminClicked.getCooldowns()[number] != 0)
                infoBoxx.Show("Cooldown!", "That ability is\non cooldown!");
            else
                infoBoxx.Show("Mana low!", "That ability requires\nmore mana!");
    }

    [RPC]
    public void phase21()
    {
        magicClicked = null;
        drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, -2);
        friendlyForward();
        phase = 1;
    }

    public void phasex2(string pot)
    {
        if ((pot == "Mana" && player.getManaPot() > 0) || (pot == "Health" && player.getHealthPot() > 0))
        {
            mask.transform.position = new Vector3(0, 0);
            friendlyForward();
            phase = 2;
            potionUsed = true;
            firstZaraminClicked = null;
            zaraminForUpdate = -1;
            potion = pot;
            drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        }
        else
            infoBoxx.Show("No potions!", "You have no potions\nof that type!\nBuy some at Statistics!");
    }

    [RPC]
    public void phase23(int number)
    {
        int dmg;

        if (potionUsed)
        {
            secondZaraminClicked = player.myZaramins[player.activeZaramins[number]];

            if (!secondZaraminClicked.isDead())
            {
                if (potion == "Mana")
                    player.decManaPot();
                else
                    player.decHealthPot();
                zaraminForUpdate = number;
                secondZaraminClicked.usePotion(potion);
                net.RPC("usePot", RPCMode.Others, new object[]{potion , zaraminForUpdate});
                string msg = "\n>>" + player.getName() + " used " + potion + " potion on " + secondZaraminClicked.getName()
                    + " and it recovered 100 points of " + potion + "!";
                net.RPC("sendMsg", RPCMode.All, msg);
            }
            else
            {
                infoBoxx.Show("Dead!", "That Zaramin is\ndead!");
                return;
            }
        }
        else
        {
            if (magicClicked.getFriendly())
            {
                secondZaraminClicked = player.myZaramins[player.activeZaramins[number]];

                if (!secondZaraminClicked.isDead())
                {
                    dmg = firstZaraminClicked.attack(magicClicked, secondZaraminClicked);
                    net.RPC("lanAttack", RPCMode.Others, new object[] { zaraminForUpdate, currentMagicIndex, number });

                    UpdateHPMana(number);
                    net.RPC("UpdateHPMana", RPCMode.Others, number + 3);

                    string msg = "\n>>" + firstZaraminClicked.getName() + " healed " + secondZaraminClicked.getName() + " with " +
                    magicClicked.getName() + " for " + -dmg + " points!";
                    net.RPC("sendMsg", RPCMode.All, msg);
                }
                else
                {
                    infoBoxx.Show("Dead!", "That Zaramin is\ndead!");
                    return;
                }
            }
            else
            {
                secondZaraminClicked = enemyZaramins[number];

                if (!secondZaraminClicked.isDead())
                {
                    if (magicClicked.getStun())
                    {
                        firstZaraminClicked.Stun(secondZaraminClicked);

                        net.RPC("LanStunZaramin", RPCMode.Others, new object[] { number, zaraminForUpdate });

                        string msg = "\n>>" + firstZaraminClicked.getName() + " stunned " + secondZaraminClicked.getName() + " for one turn!";
                        net.RPC("sendMsg", RPCMode.All, msg);
                    }
                    else
                    {
                        dmg = firstZaraminClicked.attack(magicClicked, secondZaraminClicked);
                        net.RPC("lanAttack", RPCMode.Others, new object[] { zaraminForUpdate, currentMagicIndex, number });

                        UpdateHPMana(3 + number);
                        net.RPC("UpdateHPMana", RPCMode.All, number);
                        string msg = "\n>>" + firstZaraminClicked.getName() + " attacked " + secondZaraminClicked.getName() + " with " +
                        magicClicked.getName() + " for " + dmg + " points!";
                        net.RPC("sendMsg", RPCMode.All, msg);
                    }
                }
                else
                {
                    infoBoxx.Show("Dead!", "That Zaramin is\ndead!");
                    return;
                }

            }
        }

        friendlyForward();
        drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        mask.transform.position = new Vector3(20, 0);

        phase = 3;//u fazi 3 ovaj ceka da odigra protivnik
        
        phase34();
    }

    [RPC]
    public void phase34()
    {
        UpdateHPMana(zaraminForUpdate);
        net.RPC("UpdateHPMana", RPCMode.Others, zaraminForUpdate + 3);
        phase = 4;
        if (checkPlayerWin())
        {
            winGame();
            net.RPC("loseGame", RPCMode.Others);
        }
        else
        {
            opponentsturn.Play();
            net.RPC("setPhase", RPCMode.Others, new object[] { 0 });
            net.RPC("lanUpdateZaramins", RPCMode.Others);
            selfUpdate();
            
            Timer.net.RPC("ResetTimer", RPCMode.All);
        }
    }

    [RPC]
    public void UpdateHPMana(int zaraminN)
    {
        healthFill = GameObject.Find("Zaramin" + zaraminN + "/HPMana/Health/TimeBarFill");
        manaFill = GameObject.Find("Zaramin" + zaraminN + "/HPMana/Health/TimeBarFill");

        if (zaraminN < 3)//Ako je zaramin od player-a
        {
            healthFill = GameObject.Find("FriendlyZaramins/Zaramin" + (zaraminN + 1) + "/HPMana/Health/TimeBarFill");
            manaFill = GameObject.Find("FriendlyZaramins/Zaramin" + (zaraminN + 1) + "/HPMana/Mana/TimeBarFill");
            healthFill.transform.localScale = new Vector3((length * player.myZaramins[player.activeZaramins[zaraminN]].getCurrentHealth())
                / player.myZaramins[player.activeZaramins[zaraminN]].getMaxHealth(),
                healthFill.transform.localScale.y, healthFill.transform.localScale.z);
            manaFill.transform.localScale = new Vector3((length * player.myZaramins[player.activeZaramins[zaraminN]].getCurrentMana())
                / player.myZaramins[player.activeZaramins[zaraminN]].getMaxMana(), manaFill.transform.localScale.y,
                manaFill.transform.localScale.z);
            checkFriendlyDeadAlpha(zaraminN);
        }
        else//Zaramin je protivnikov
        {
            healthFill = GameObject.Find("HostileZaramins/Zaramin" + (zaraminN + 1) + "/HPMana/Health/TimeBarFill");
            manaFill = GameObject.Find("HostileZaramins/Zaramin" + (zaraminN + 1) + "/HPMana/Mana/TimeBarFill");
            Debug.Log("Usao: " + zaraminN);
            healthFill.transform.localScale = new Vector3((length * enemyZaramins[zaraminN - 3].getCurrentHealth())
                / enemyZaramins[zaraminN - 3].getMaxHealth(),
                healthFill.transform.localScale.y, healthFill.transform.localScale.z);
            manaFill.transform.localScale = new Vector3((length * enemyZaramins[zaraminN - 3].getCurrentMana())
                / enemyZaramins[zaraminN - 3].getMaxMana(),
                manaFill.transform.localScale.y, manaFill.transform.localScale.z);
            checkHostileDeadAlpha(zaraminN - 3);
        }
    }
    
    [RPC]
    private void drawDrawer()//Iscrtavanje magija
    {
        firstMagic.sprite = firstZaraminClicked.getMagics()[0].iconOn;

        if (firstZaraminClicked.getCooldowns()[1] < 1)//Da li su na cooldownu ili ne
            secondMagic.sprite = firstZaraminClicked.getMagics()[1].iconOn;
        else
            secondMagic.sprite = firstZaraminClicked.getMagics()[1].iconOff;

        if (firstZaraminClicked.getCooldowns()[2] < 1)
            thirdMagic.sprite = firstZaraminClicked.getMagics()[2].iconOn;
        else
            thirdMagic.sprite = firstZaraminClicked.getMagics()[2].iconOff;
    }

    public string magicInfo(int n)
    {
        return firstZaraminClicked.getMagics()[n].getInfo();
    }

    [RPC]
    private void Paint()//Iscrtavanje Zaramina koji su u borbi
    {
        int i;

        for (i = 1; i < 4; i++)
        {
            Debug.Log("FriendlyZaramins/Zaramin" + i);
            GameObject.Find("FriendlyZaramins/Zaramin" + i).GetComponent<SpriteRenderer>().sprite =
                player.myZaramins[player.activeZaramins[i - 1]].small;
        }

        for (i = 4; i < 7; i++)
            GameObject.Find("HostileZaramins/Zaramin" + i).GetComponent<SpriteRenderer>().sprite =
                enemyZaramins[(i - 1) % 3].small;
    }

    [RPC]
    private void friendlyForward()
    {
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x, friendlyZaramins.transform.position.y, -2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x, hostileZaramins.transform.position.y, 2);
    }
    [RPC]
    private void hostileForward()
    {
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x, friendlyZaramins.transform.position.y, 2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x, hostileZaramins.transform.position.y, -2);
    }
    
    [RPC]
    void sendZaramin(int poz, string name, string type, string dht, int experience)
    {
        enemyZaramins[poz] = player.getZaramin(name, type, dht, experience, magicPanel);
    }

    void OnConnectedToServer ()//Client
    {
        for (int i = 0; i < 3; i++ )
        {
            net.RPC("sendZaramin", RPCMode.Others, new object[] { i, player.myZaramins[player.activeZaramins[i]].getName(),
                player.myZaramins[player.activeZaramins[i]].getRace(), player.myZaramins[player.activeZaramins[i]].getType(),
                player.myZaramins[player.activeZaramins[i]].getExperience() });
        }

        net.RPC("Paint", RPCMode.Others);
        

        GameObject.Find("Canvas/Create").SetActive(false);
        GameObject.Find("Canvas/Join").SetActive(false);
        GameObject.Find("Destroyer").SetActive(false);
        GameObject.Find("Music").transform.position = new Vector3(0, 3.5f, -5);
        GameObject.Find("Music").transform.localScale = new Vector3(.5f, .5f, 1);
        GameObject.Find("Tutorial?").SetActive(false);
        GameObject.Find("MainMenuButton").SetActive(false);

        chat.SetActive(true);
        net.RPC("oppNameLvl", RPCMode.Others, new object[] { player.getName(), player.getLevel() });

          
    }

    void OnPlayerConnected ()//Server
    {
        for (int i = 0; i < 3; i++)
        {
            net.RPC("sendZaramin", RPCMode.Others, new object[] { i, player.myZaramins[player.activeZaramins[i]].getName(),
                player.myZaramins[player.activeZaramins[i]].getRace(), player.myZaramins[player.activeZaramins[i]].getType(),
                player.myZaramins[player.activeZaramins[i]].getExperience() });
        }

        net.RPC("Paint", RPCMode.Others);

        background.randomBackground();
        net.RPC("setBack", RPCMode.Others, background.GetRace());

        GameObject.Find("Canvas/Create").SetActive(false);
        GameObject.Find("Canvas/Join").SetActive(false);
        GameObject.Find("Destroyer").SetActive(false);
        GameObject.Find("Music").transform.position = new Vector3(0, 3.5f, -5);
        GameObject.Find("Music").transform.localScale = new Vector3(.5f, .5f, 1);
        GameObject.Find("Tutorial?").SetActive(false);
        GameObject.Find("MainMenuButton").SetActive(false);

        chat.SetActive(true);

        GameObject.Find("InfoBoxx").transform.position = new Vector3(-20, 0, -11.03f);

        //net.RPC("prepareZaramins", RPCMode.Others);
        
        if (UnityEngine.Random.RandomRange(0, 1) < 0.5)
        {
            net.RPC("setPhase", RPCMode.Others, 3);
            setPhase(0);
            yourturn.Play();
            net.RPC("opponentsTurn", RPCMode.Others);
        }
        else
        {
            net.RPC("setPhase", RPCMode.Others, 0);
            setPhase(3);
            opponentsturn.Play();
            net.RPC("yourTurn", RPCMode.Others);
        }

        net.RPC("oppNameLvl", RPCMode.Others, new object[] { player.getName(), player.getLevel() });

        Timer.net.RPC("StartTimer", RPCMode.All);
    }

    [RPC]
    public void setPhase (int phase)
    {
        this.phase = phase;
    }

    [RPC]
    public void lanAttack (int firstZaraminIndex,int magicIndex,int secondZaraminIndex)
    {
        enemyZaramins[firstZaraminIndex].attack(enemyZaramins[firstZaraminIndex].getMagics()[magicIndex],
            (enemyZaramins[firstZaraminIndex].getMagics()[magicIndex].getFriendly() ?
            enemyZaramins[secondZaraminIndex] : player.myZaramins[player.activeZaramins[secondZaraminIndex]]));
    }

    [RPC]
    public void lanSetBuffed (int zaraminIndex, int duration)
    {
        enemyZaramins[zaraminIndex].SetBuffed(duration);
    }

    [RPC]
    public void lanDoSelf (int zaraminIndex)
    {
        enemyZaramins[zaraminIndex].DoSelf();
    }

    [RPC]
    public void lanUpdateZaramins ()
    {
        for (int i = 0; i < 3; i++)
        {
            player.myZaramins[player.activeZaramins[i]].updateZaramin();
            enemyZaramins[i].unstunZaramin();
        }

        yourturn.Play();
    }

    //Zovem rucno
    [RPC]
    public void selfUpdate()
    {
        mask.transform.position = new Vector3(20, 0);
        drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);

        for (int i = 0; i < 3; i++)
        {
            player.myZaramins[player.activeZaramins[i]].unstunZaramin();
            enemyZaramins[i].updateZaramin();
        }

        opponentsturn.Play();
    }

    public void OnPlayerDisconnected()
    {
        if (!gameFinished)
        {
            for (int i = 0; i < 3; i++)
            {
                player.myZaramins[player.activeZaramins[i]].restoreAll();
            }
            infoBox.Show("Disconnected!", "Your opponent\ngot disconnected!", true);             
        }
    }

    public void OnDisconnectedFromServer ()
    {
        if (!gameFinished)
        {
            for (int i = 0; i < 3; i++)
            {
                player.myZaramins[player.activeZaramins[i]].restoreAll();
            }
            infoBox.Show("Disconnected!", "Your opponent\ngot disconnected!", true);            
        }
    }

    private bool checkPlayerWin()
    {
        return (enemyZaramins[0].isDead() && enemyZaramins[1].isDead() && enemyZaramins[2].isDead());
    }

    public void winGame()
    {
        gameFinished = true;
        GameObject.Find("EndBox").transform.position = new Vector3(0, 0, -8);
        GameObject.Find("EndBox/Win").GetComponent<TextMesh>().text = "YOU WON!";
        GameObject.Find("EndBox/WinText").GetComponent<TextMesh>().text = "Experience earned : 300\n\nGold earned: 100";
        Timer.StopTimer();
        player.incGold(100);
        endBattle(300, 300 / 3);
        script.BattleEnds();
    }

    [RPC]
    public void loseGame()
    {
        gameFinished = true;
        GameObject.Find("EndBox").transform.position = new Vector3(0, 0, -8);
        GameObject.Find("EndBox/Win").GetComponent<TextMesh>().text = "YOU LOST!";
        GameObject.Find("EndBox/WinText").GetComponent<TextMesh>().text = "Experience earned : 150\n\nGold earned: 50";
        Timer.StopTimer();
        player.incGold(50);
        endBattle(150, 150 / 3);
        script.BattleEnds();
    }

    private void endBattle(int playerExp, int zaraminExp)
    {
        player.changeExperience(playerExp);
        for (int i = 0; i < 3; i++)
        {
            player.myZaramins[player.activeZaramins[i]].changeExperience(zaraminExp);
            player.myZaramins[player.activeZaramins[i]].restoreAll();
        }
    }

    [RPC]
    public void LanStunZaramin(int index, int enemyIndex)
    {
        enemyZaramins[enemyIndex].Stun(player.myZaramins[player.activeZaramins[index]]);
    }

    [RPC]
    private void prepareZaramins()
    {
        for (int i = 0; i < 3; i++)
        {
            if (player.myZaramins[player.activeZaramins[i]].getRace() == background.GetRace())
            {
                player.myZaramins[player.activeZaramins[i]].setMaxHealth();
            }

            if (enemyZaramins[i].getRace() == background.GetRace())
            {
                enemyZaramins[i].setMaxHealth();
            }
        }
    }

    [RPC]
    private void setBack(string race)
    {
        background.setBackground(race);
        net.RPC("prepareZaramins", RPCMode.Others);
        prepareZaramins();
    }

    [RPC]
    private void sendMsg(string msg)
    {
        log.text += msg;
    }

    [RPC]
    private void yourTurn()
    {
        yourturn.Play();
    }

    [RPC]
    private void opponentsTurn()
    {
        opponentsturn.Play();
    }

    [RPC]
    private void usePot(string pot, int n)
    {
        enemyZaramins[n].usePotion(pot);
    }

    [RPC]
    private void oppNameLvl(string name, int lvl)
    {
        opponentName = name;
        GameObject.Find("PTO/Opponent/OpponentText").GetComponent<TextMesh>().text = opponentName + "\nLevel: " + lvl;
    }
    void OnFailedToConnect(NetworkConnectionError error)
    {
        if (error == NetworkConnectionError.InvalidPassword)
        {
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("Wrong password!", "Please reenter\npassword!");
        }
        else
        {
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("Network error!", "Please try again\nlater!");
        }
    }

    private void checkFriendlyDeadAlpha(int n)
    {
        if (player.myZaramins[player.activeZaramins[n]].isDead())
        {
            GameObject.Find("FriendlyZaramins/Zaramin" + (n + 1)).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
        }
    }
    private void checkHostileDeadAlpha(int n)
    {
        if (enemyZaramins[n].isDead())
        {
            GameObject.Find("HostileZaramins/Zaramin" + (n + 4)).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
        }
    }
}
