﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Threading;

public class JoinGameButtonScript : MonoBehaviour
{

    public ConnectionTesterStatus status;
    public bool connectionEstablished;
    public LanGameControlScript lanGameController;
    public SoundManagerScript script;
    //Sprite za on i off stanje
    public Sprite on, off;

	// Use this for initialization
	void Start () {

        connectionEstablished = false;
        lanGameController = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {

        
	
	}

    void OnMouseUpAsButton ()
    {
        script.onClickSound();
        ConnectToHost(GameObject.Find("Canvas/Join/ClientIpAddress").GetComponent<InputField>().text,
                            GameObject.Find("Canvas/Join/PasswordField").GetComponent<InputField>().text);
        
    }
    void ConnectToHost(string ip, string password)
    {
        NetworkConnectionError networkStatus;

        networkStatus = Network.Connect(ip, 6000, password);
        
        if (networkStatus == NetworkConnectionError.NoError)
        {
        
        }
        else if (networkStatus == NetworkConnectionError.InvalidPassword)
        {
            GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("Wrong password!", "Please reenter\npassword!");
        }
        else if (networkStatus == NetworkConnectionError.AlreadyConnectedToServer)
        {

        }
        else if (networkStatus == NetworkConnectionError.AlreadyConnectedToAnotherServer)
        {

        }
        else if (networkStatus == NetworkConnectionError.ConnectionFailed)
        {

        }
        else if (networkStatus == NetworkConnectionError.ConnectionBanned)
        {

        }
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(1, 1, 1);
    }

}
