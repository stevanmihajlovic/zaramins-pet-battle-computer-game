﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LevelScript : MonoBehaviour {

	public TextMesh bossName;
	public TextMesh bossInfo;
    
    public GameControlScript gameController;
    public LevelSceneControllerScript skripta;
    public PlayerScript player;
    public SoundManagerScript script;
 
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()
	{
        script.onClickSound();
        int level;
        if (this.name == "Level 1")
        {
            level = 1;
        }
        else if (this.name == "Level 2")
        {
            level = 2;
        }
        else if (this.name == "Level 3")
        {
            level = 3;
        }
        else
            level = 4;

        skripta.selectLevel(level);       
		displayInfo(skripta.selectedLevel);
	}

	void displayInfo(int levelNo)
	{
        string[] lines = File.ReadAllLines(Application.dataPath + "/Levels/" + levelNo.ToString()+".txt");
        string[] lines_for_new = lines[20].Split(';');
        bossName.text = lines[19];
        bossInfo.text = "";
        for (int i = 0; i < lines_for_new.Length; i++)
        {
            bossInfo.text += lines_for_new[i];
            bossInfo.text += "\n";
        }
    }

	
}
