﻿using UnityEngine;
using System.Collections;

public class LANInfoBoxScript : MonoBehaviour
{
    public TextMesh naslov, tekst;
    public bool mode;
    public Sprite iks;
    public Sprite ok;
	
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show(string nas, string txt,bool mode)//mode=false ---->sa x-om, mode=true ---->sa ok
    {
        this.mode = mode;

        if (!mode)       
            (GameObject.Find("InfoBoxx/Done")).GetComponent<SpriteRenderer>().sprite = iks;
        else
            (GameObject.Find("InfoBoxx/Done")).GetComponent<SpriteRenderer>().sprite = iks;
        
        naslov.text = nas;
        tekst.text = txt;
        transform.position = new Vector3(0, 0, -9.8f);        
    }
}
