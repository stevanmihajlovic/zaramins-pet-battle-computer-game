﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEditor;

public class MyZaraminsButtonsScript : MonoBehaviour
{
    private PlayerScript player;    
    public Sprite on, off;//Sprite za on i off stanje
    private InfoBoxScript infoBox;
    public SoundManagerScript script;

	void Start () 
    {
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	void Update () 
    {}

    void OnMouseUpAsButton()
    {
        script.onClickSound();
        if (name == "AddZaramin")//Dodavanje novog Zaramina
        {
            if (player.getZaraminsCount() == 10)//Provera da li je tim pun
            {
                infoBox.Show("Maximum reached!", "You cannot have more than ten (10)\nactive Zaramins!");
            }
            else
            {
                GameObject.Find("AddZaraminPanel").transform.position = new Vector3(0, 0, -3.5f);//Pomeranje panela
                GameObject.Find("Canvas/InputField").transform.localScale = new Vector3(0.9f, 1, 1);//Obnavljanje InputFielda
            }
        }

        if (name == "AddToTeam")//Dodavanje Zaramina u tim
        {
            GameObject.Find("MyZaraminsController").GetComponent<MyZaraminsController>().AddToTeam();
        }


        if (name == "RemoveFromTeam")//Izbacivanje Zaramina iz tima
        {
            GameObject.Find("MyZaraminsController").GetComponent<MyZaraminsController>().RemoveFromTeam();
        }

        if (name == "DeleteZaramin")//Brisanje Zaramina
        {
            GameObject.Find("MyZaraminsController").GetComponent<MyZaraminsController>().DeleteZaramin();
        }
        

    }
    void OnMouseEnter()//Pocetak hovera
    { 
        GetComponent<SpriteRenderer>().sprite = on;
        if (name == "DeleteZaramin")
            gameObject.transform.localScale = new Vector3(1.15f, 1.20f, 1);
        else
            gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

    void OnMouseExit()//Kraj hovera
    {  
        GetComponent<SpriteRenderer>().sprite = off;
        if (name == "DeleteZaramin")
            gameObject.transform.localScale = new Vector3(0.8f, 0.85f, 1);
        else
            gameObject.transform.localScale = new Vector3(1, 1, 1);      
    }
}
