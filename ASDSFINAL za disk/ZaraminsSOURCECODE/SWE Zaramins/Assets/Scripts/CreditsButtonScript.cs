﻿using UnityEngine;
using System.Collections;

public class CreditsButtonScript : MonoBehaviour
{
    //Sprite za on i off stanje
    public Sprite on, off;
    public GameObject credits;
    public SoundManagerScript script;

    void Start()
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        credits.transform.position = new Vector3(0, 0, -2);
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(3, 3, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }
}
