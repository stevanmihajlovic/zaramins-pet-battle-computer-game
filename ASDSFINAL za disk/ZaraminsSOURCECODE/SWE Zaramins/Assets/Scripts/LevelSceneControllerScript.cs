﻿using UnityEngine;
using System.Collections;

public class LevelSceneControllerScript : MonoBehaviour {

    public PlayerScript player;
    public Sprite locked, unlocked, selected;
    private int maxLevel = 4; //jebala te kamila
    private int playerMaxLevel;
    public int selectedLevel;

	// Use this for initialization
	void Start () {

        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        playerMaxLevel = player.getLevelReached();
        setAvaiableLevels(playerMaxLevel);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void setAvaiableLevels(int playerMax)
    {
        GameObject level_locked;//, level_unlocked;
        string levelName;       
               
        for (int i = 1; i <= maxLevel; i++)
        {
            levelName = "Level " + i.ToString();
            level_locked = GameObject.Find(levelName);
           
            if (i <= playerMax)
            {
                level_locked.GetComponent<SpriteRenderer>().sprite = unlocked;
            }
            else
            {
                level_locked.GetComponent<SpriteRenderer>().sprite = locked;
            }
        }

    }
    public void selectLevel (int levelNo)
    {
       if (levelNo <= playerMaxLevel)
       {
           unselectAllLevels();
           GameObject.Find("Level " + levelNo.ToString()).GetComponent<SpriteRenderer>().sprite = selected;
           selectedLevel = levelNo;
       }
       else
       {
           GameObject.Find("InfoBox").GetComponent<InfoBoxScript>().Show("Level locked!", "You have to win all\nprevous battles\nbefore play this.");
       }
    }

    public void updatePlayerMaxLevel ()
    {
        this.maxLevel = player.getLevelReached();
        setAvaiableLevels(maxLevel);
    }
    public void unselectAllLevels ()
    {
        for (int i = 1; i<=playerMaxLevel; i++)
        {
            GameObject.Find("Level " + i.ToString()).GetComponent<SpriteRenderer>().sprite = unlocked;
        }
       
    }

}
