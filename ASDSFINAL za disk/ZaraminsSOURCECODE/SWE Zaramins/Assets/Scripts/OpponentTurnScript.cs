﻿using UnityEngine;
using System.Collections;

public class OpponentTurnScript : MonoBehaviour
{
    public GameControlScript gameController;

    void Start()
    {}

    void Update()
    {}

    IEnumerator endAnimation()
    {
        yield return new WaitForSeconds((int)UnityEngine.Random.RandomRange(1, 15));
        gameController.enemyPlay();
    }
}
