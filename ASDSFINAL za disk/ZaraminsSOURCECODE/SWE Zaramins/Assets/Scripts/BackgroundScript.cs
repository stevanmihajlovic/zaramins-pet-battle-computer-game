﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour
{
    public Sprite[] niz;
    private string race;

    void Start()
    {}

    void Update()
    {}

    public string GetRace()
    {
        return race;
    }

    public void setBackground(string racee)
    {
        this.race = racee;
        if (race == "Fire")
            GetComponent<SpriteRenderer>().sprite = niz[0];
        else
            if (race == "Earth")
                GetComponent<SpriteRenderer>().sprite = niz[1];
            else
                if (race == "Water")
                    GetComponent<SpriteRenderer>().sprite = niz[2];
                else
                    GetComponent<SpriteRenderer>().sprite = niz[3];
    }

    public void randomBackground()
    {
        float i = UnityEngine.Random.RandomRange(0f, 1f);

        if (i <= 0.25f)
        {
            race = "Fire";
            GetComponent<SpriteRenderer>().sprite = niz[0];
        }
        else
            if (i <= 0.5f)
            {
                race = "Earth";
                GetComponent<SpriteRenderer>().sprite = niz[1];
            }
            else
                if (i <= 0.75f)
                {
                    race = "Water";
                    GetComponent<SpriteRenderer>().sprite = niz[2];
                }
                else
                {
                    race = "Dark";
                    GetComponent<SpriteRenderer>().sprite = niz[3];
                }	
    }

}
