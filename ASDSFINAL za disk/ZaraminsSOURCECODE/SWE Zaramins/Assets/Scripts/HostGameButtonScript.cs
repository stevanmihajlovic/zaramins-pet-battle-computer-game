﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HostGameButtonScript : MonoBehaviour
{
    //Sprite za on i off stanje
    public Sprite on, off;
    public SoundManagerScript script;

	// Use this for initialization
	void Start () {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton ()
    {
        if (!Network.isServer)//bogumi ne znam sta treba :D
        {
            script.onClickSound();
            StartServer(GameObject.Find("Canvas/Create/ServerPassword").GetComponent<InputField>().text);
            (GameObject.Find("InfoBoxx").GetComponent<LANInfoBoxScript>()).Show("Game Created", "Waiting for\nopponent to connect", false);
        }

    }

    void StartServer (string password)
    {
        Network.incomingPassword = password;
        bool useNat = !Network.HavePublicAddress();
        Network.InitializeSecurity();
        Network.InitializeServer(5, 6000, useNat);
        Debug.Log("Slusam!");
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(1, 1, 1);
    }
}
