﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class MagicPanelScript : MonoBehaviour
{
    //Niz za magije
    private Magic[] magicArray;
    //Ukupan broj magija
    private int count;
    
    //Ovaj objekat bi trebalo da postoji kroz sve scene
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    //Count je ukupan broj magija, f-ja inicijalizuje magije
    void Start()
    {
        count = 7;
        MagicPanel();    
    }
    
    void Update()
    {}

    //Getter za niz magija
    public Magic[] getMagics()
    {
        return magicArray;
    }
    
    //F-ja za inicijalizaciju svih magija, cita iz magics.txt
    public void MagicPanel()
    {
        StreamReader f = new StreamReader(Application.dataPath + "/Magic/magics.txt");
        magicArray = new Magic[count];

        for (int i = 0; i < count; i++)
        {
            string name = f.ReadLine();
            string mana = f.ReadLine();
            string damage = f.ReadLine();
            string friendly = f.ReadLine();
            string cooldown = f.ReadLine();
            string self = f.ReadLine();
            string stun = f.ReadLine();
            string info1 = f.ReadLine();
            string info2 = f.ReadLine();

            f.ReadLine();//New line

            //Pozivanje konstruktora za svaku magiju ponaosob
            magicArray[i] = new Magic (name, Convert.ToInt32(mana), Convert.ToInt32(damage), Convert.ToBoolean(friendly),
                                      Convert.ToInt32(cooldown), Convert.ToBoolean(self), Convert.ToBoolean(stun), info1, info2);
        }
        f.Close();
    }
}



public class Magic
{
    //DODAJTE INFO
    private string name;
    private string info1,info2;
    private int manaCost;
    private int damageDeal;
    private bool friendly;
    private int cooldown;
    private bool self;
    private bool stun;
    public Sprite iconOn,iconOff;

    public Magic(string name, int manaCost, int damageDeal, bool friendly, int cooldown, bool self, bool stun, string info1, string info2)
    {
        this.name = name;
        this.manaCost = manaCost;
        this.damageDeal = damageDeal;
        this.friendly = friendly;
        this.cooldown = cooldown;
        this.self = self;
        this.stun = stun;
        this.info1 = info1;
        this.info2 = info2;
        //Izvlaci Sprite za ikonicu
        getTexture();
    }

    //Getteri za SVE :D

    public string getInfo()
    {
        return "Name: " + name + "\nManaCost: " + manaCost + "\nCooldown: " + cooldown + "\nInfo: " + info1 + "\n" + info2;
    }

    public int doMagic()
    {
        return getDamage();
    }

    public bool getType()
    {
        return friendly;
    }

    public bool getSelf()
    {
        return this.self;
    }

    public string getName()
    {
        return name;
    }

    public int getMana()
    {
        return manaCost;
    }

    public int getDamage()
    {
        return damageDeal;
    }

    public bool getFriendly()
    {
        return friendly;
    }

    public bool getStun()
    {
        return stun;
    }

    public int getCooldown()
    {
        return cooldown;
    }

    
    public void getTexture()
    {
        int i = 0, j = 0;
        Sprite [] niz = Resources.LoadAll<Sprite>("ZaraminsFinal/MagicMini");
        while (j < 2 && niz[i] != null)
            if (niz[i].name == (name + "On"))
            {
                iconOn = niz[i++];
                j++;
            }
            else
                if (niz[i].name == (name + "Off"))
                {
                    iconOff = niz[i++];
                    j++;
                }
                else
                    i++;
    }
}
