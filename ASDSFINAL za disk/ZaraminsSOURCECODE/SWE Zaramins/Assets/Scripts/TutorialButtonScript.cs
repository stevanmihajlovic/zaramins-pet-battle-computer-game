﻿using UnityEngine;
using System.Collections;

public class TutorialButtonScript : MonoBehaviour
{
    //Sprite za on i off stanje
    public Sprite on, off;
    GameObject box;
    public SoundManagerScript script;

    void Start()
    {
        box = GameObject.Find("Statistics");
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        GameObject.Find("Statistics/StatText").GetComponent<TextMesh>().text =
            GameObject.Find("Player").GetComponent<PlayerScript>().getPlayerInfo();
        box.transform.position = new Vector3(0, 0, -2);
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(3, 3, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }
}
