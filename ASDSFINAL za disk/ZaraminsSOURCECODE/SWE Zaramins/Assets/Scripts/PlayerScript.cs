﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class PlayerScript : MonoBehaviour, IFile
{
    //Igraceve karakteristike
	private string username;
    private int experience, playerLevel, zaraminsCount, levelReached, gold, healthPot, manaPot, won, lost;
	public Zaramin[] myZaramins;
	public int[] activeZaramins;
	private MagicPanelScript magicPanel;
    public int currentLevel;
    //public SpriteRenderer ne; //Mozda ovo da bude ikonica igraca?

    //Player ne sme da se unisti prelaskom u drugu scenu
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }


	void Start ()
	{
        //Radi inicijalizacije Zaramina
        magicPanel = GameObject.Find("MagicPanel").GetComponent<MagicPanelScript>();
        //Write(Application.dataPath + "/Players/" + username + ".txt");
	}
	
    
    void Update ()
	{}
    
    
    public void PlayerScriptConstructor (string usernamee)//Inicijalizacija igraca
	{
		username = usernamee;

        if (File.Exists (Application.dataPath + "/Players/" + username + ".txt"))

            Load (Application.dataPath + "/Players/" + username + ".txt");//Ucitava karakteristike igraca iz .txt o svemu
        
        else//Ako je u pitanju novi igrac
        {
            playerLevel = 0;
            experience = 0;
            gold = 0;
            zaraminsCount = 0;
            healthPot = 5;
            manaPot = 5;
            won = 0;
            lost = 0;
            myZaramins = null;
            activeZaramins = null;
            levelReached = 1;
            activeZaramins = new int[3];
            myZaramins = new Zaramin[10];//Za svaki slucaj, da se ne majemo kasnije

            activeZaramins[0] = -1;//Tim ima inicijalne vrednosti od -1
            activeZaramins[1] = -1;
            activeZaramins[2] = -1;

            Write(Application.dataPath + "/Players/" + username + ".txt");
        }

        levelCalculate();//Izracunavanje levela na osnovu exp
	}

    public void changeExperience(int newexperience)//Dodavanje exp
    {
        experience += newexperience;
        levelCalculate();//Preracunavanje levela
    }

    public void levelCalculate()//Izracunavanje levela na osnovu exp
    {
        playerLevel = experience / 1000 + 1;
    }

    //Inicijalizacija Zaramina
	public Zaramin getZaramin (string name,string type,string dht,int experience, MagicPanelScript magicPanel)
    {
        //racunanje bonusa Zaramina
        int dps = (dht == "DPS") ? 20 : 0;
        int healer = (dht == "Healer") ? 20 : 0;
        int tank = (dht == "Tank") ? 20 : 0;
        switch (type)
        {
            //pozivanje konstruktora Zaramina u zavisnosti koji je parametar prosledjen
            case "Fire": return new FireZaramin(name, dps, healer, tank, experience, magicPanel.getMagics());
            case "Earth": return new EarthZaramin(name, dps, healer, tank, experience, magicPanel.getMagics());
            case "Dark": return new DarkZaramin(name, dps, healer, tank, experience, magicPanel.getMagics());
            case "Water": return new WaterZaramin(name, dps, healer, tank, experience, magicPanel.getMagics());
            default: return null;
        }
    }


    public bool Load(string id)//Ucitava karakteristike igraca iz .txt o svemu
    {
        try
        {
            string[] lines = System.IO.File.ReadAllLines(id);
			
            if (lines == null)
				return false;
            
			playerLevel = Convert.ToInt32(lines[0]);
			experience = Convert.ToInt32(lines[1]);
			gold = Convert.ToInt32(lines[2]);
            levelReached = Convert.ToInt32(lines[3]);
            healthPot = Convert.ToInt32(lines[4]);
            manaPot = Convert.ToInt32(lines[5]);
            won = Convert.ToInt32(lines[6]);
            lost = Convert.ToInt32(lines[7]);
            zaraminsCount = Convert.ToInt32(lines[8]);
            myZaramins = new Zaramin[10];//Za svaki slucaj, da se ne majemo kasnije
            activeZaramins = new int[3];

            //Za slucaj da ima 0 Zaramina
            for (int i = 0; i < 3; i++)
                activeZaramins[i] = -1;

            //ako ima Zaramine
            if (zaraminsCount > 0)
            {
                string[] zaramins_exp = lines[9].Split(' ');
                string[] zaramin_name = lines[10].Split(' ');
                string[] type = lines[11].Split(' ');
                string[] dht = lines[12].Split(' ');
                string[] aktivni = lines[13].Split(' ');

                
                
                for (int i = 0; i < zaraminsCount; i++)
                   
                    myZaramins[i] = getZaramin(zaramin_name[i], type[i], dht[i], Convert.ToInt32(zaramins_exp[i]), magicPanel);

                //Prepravljeno i radi
                for (int i = 0; i < 3; i++)
                    activeZaramins[i] = Convert.ToInt32(aktivni[i]);
            }            
			return true;
        }

        catch (Exception e)
        {            
            Debug.Log("Ufatio sam ga.\n" + e.Message);
			return false;
        }

        finally
        {
            //Debug.Log("Ovde cu se nerviram!\n");
        }
    }

    public void incLevelReached()
    {
        if (levelReached < 5)
            levelReached++;
    }
    public void incHealthPot()
    {
        healthPot++;
    }

    public void incManaPot()
    {
        manaPot++;
    }

    public void decHealthPot()
    {
        healthPot--;
    }

    public void decManaPot()
    {
        manaPot--;
    }

    public bool InTeam(int n)//Da li je myZaramins[n] u timu?
    {
        if (n != activeZaramins[0] && n != activeZaramins[1] && n != activeZaramins[2])
            return false;
        else
            return true;    
    }

    public void TeamRemove(int n)//Izbacivanje iz myZaramins[n] tima
    {
        int i = 0;

        while (activeZaramins[i++] != n) ;

        activeZaramins[--i] = -1;
    }

    public void TeamJoin(int n)//Ubacivanje myZaramins[n] u tim
    {
        int i = 0;
        while (activeZaramins[i++] != -1) ;
        activeZaramins[--i] = n;
    }

    public bool TeamFull()//Provera da li je tim pun
    {
        int n = -1;
        if (n != activeZaramins[0] && n != activeZaramins[1] && n != activeZaramins[2])
            return true;
        else
            return false; 
    }


    public void DeleteZaramin(int n)//Brisanje Zaramina
    {
        int i, j;
        myZaramins[n] = null;
        for (j = 0; j < 3; j++)//Provera da li je u timu
            
            if (activeZaramins[j] == n)
            {
                TeamRemove(n);
                break;
            }

        if (n < zaraminsCount)//Presaltavanje unazad
        {
            i = n + 1;
            while (i < zaraminsCount)
            {
                for (j = 0; j < 3; j++)//Provera da li je u timu

                    if (activeZaramins[j] == i)
                    {
                        activeZaramins[j] = i - 1;
                        break;
                    }

                myZaramins[i - 1] = myZaramins[i];

                i++;
            }
            myZaramins[i - 1] = null;
        }

        zaraminsCount--;//Smanji ukupan broj
    }

    public string getPlayerInfo()
    {
        return "Name: " + username + "\nLevel: " + playerLevel + "\nWon: " + won + "\nLost: " + lost + "\nWin Percentage: " +
            Mathf.Round(won * 100f / (won + lost)) + "%\nGold: " + gold + "\nPotions:      x" + healthPot + "         x" + manaPot;
    }

    public void buyPot(string pot)
    {
        if (pot == "Mana")
            manaPot++;
        else
            healthPot++;
        gold -= 300;
    }


    public bool Write(string id)//Upis u .txt
    {
        
		using (StreamWriter f = new StreamWriter (Application.dataPath+"/Players/"+username+".txt", false))
        {
			f.WriteLine (playerLevel);
			f.WriteLine (experience);
			f.WriteLine (gold);
            f.WriteLine (levelReached);
            f.WriteLine (healthPot);
            f.WriteLine (manaPot);
            f.WriteLine (won);
            f.WriteLine (lost);
			f.WriteLine (zaraminsCount);
                
            for (int i = 0; i < zaraminsCount; i++)
                f.Write (myZaramins[i].getExperience() + " ");

            f.WriteLine();

            for (int i = 0; i < zaraminsCount; i++)
                f.Write(myZaramins[i].getName() + " ");

            f.WriteLine();

            for (int i = 0; i < zaraminsCount; i++)
                f.Write(myZaramins[i].getRace() + " ");

            f.WriteLine();

            for (int i = 0; i < zaraminsCount; i++)
                f.Write(myZaramins[i].getType() + " ");

            f.WriteLine();

            for (int i = 0; i < 3; i++)
                f.Write(activeZaramins[i] + " ");
            
        return true;

	    }

    }

    //Getteri

    public void incWins()
    {
        won++;
    }

    public int getManaPot()
    {
        return manaPot;
    }

    public int getHealthPot()
    {
        return healthPot;
    }

    public int getLevel()
    {
        return playerLevel;
    }

    public MagicPanelScript getMagicPanel()
    {
        return magicPanel;
    }

    public int getZaraminsCount()
    {
        return zaraminsCount;
    }

    public string getName()
    {
        return username;
    }

    public int getPlayerLevel ()
    {
        return playerLevel;
    }

    public int getGold ()
    {
        return gold;
    }

    public void incGold(int n)
    {
        gold += n;
    }

    public void decGold(int n)
    {
        gold -= n;
    }

    public int getLevelReached ()
    {
        return levelReached;
    }

    //Setteri
    public void incrementZaraminsCount()
    {
        zaraminsCount++;
    }
    
}


public class CoreZ//Jezgro Zaramina
{
    //Bonusi Zaramina
    protected int dpsBonus, healerBonus, tankBonus;
    
    public CoreZ()
    {
        dpsBonus = healerBonus = tankBonus = 0;
    }

    public CoreZ(int db, int hb, int tb)//Konstruktor jezgra
    {
        dpsBonus = db;
        healerBonus = hb;
        tankBonus = tb;
    }

    public string getType()//Getter za tip Zaramina
    {
        if (dpsBonus != 0)
            return "DPS";
        else
            if (healerBonus != 0)

                return "Healer";

            else return "Tank";
    }
}


public class Zaramin : CoreZ, IFile
{
    protected string name;
    protected int experience, level, maxHealth, currentHealth, maxMana, currentMana, buffed;
    protected Magic[] magics;
    protected int[] cooldowns;
    public Sprite main, small;
    protected bool stunned;

    //Konstruktor Zaramina
    public Zaramin(string name, int dps, int healer, int tank, int experience) : base(dps, healer, tank)
    {
        magics = new Magic[3];//Svaki ima 3 magije

        this.name = name;
        this.experience = experience;

        calculateLevel();//Izracunava level na osnovu exp

        Load(Application.dataPath + "/Zaramins/default.txt");//KORISTIMO ZA HP I MANU, PROMENITI!

        cooldowns = new int[3];

        resetCooldown();//Resetuje cooldown-e]

        buffed = 0;
    }

    public void restoreAll()//Posle borbe
    {
        currentHealth = maxHealth = 200;
        currentMana = maxMana;
        resetCooldown();
    }

    public int takeDamage (int damage)//Primanje dmg-a
    {
        int dmg, heal;
        dmg = (int)(damage * (100 - tankBonus) / 100f);
        heal = (int)(damage * (100 + tankBonus) / 100f);

        if (currentHealth - heal > this.maxHealth)//Ako je zahilan preko maksimalnog hpa
        {
            currentHealth = maxHealth;
            return heal;
        }
        else

            if (currentHealth - dmg < 0)//Ako ga utepaju
            {
                currentHealth = 0;
                return dmg;
            }

            else

                if (damage > 0)
                {
                    currentHealth -= dmg;//Dmg
                    return dmg;
                }
                else
                {
                    currentHealth -= heal;//Heal
                    return heal;
                }
    }

    public int getMagicIndex(Magic m)//Za postavljanje cooldown-a na magiju nakon napada
    {
        for (int i = 0; i < 3; i++)
            if (magics[i] == m)
                return i;
        return -1;
    }

    public void updateZaramin()//Nakon pocetka svakog poteza
    {
        for (int i = 0; i < 3; i++)
        {
            if (cooldowns[i] > 0)
                cooldowns[i]--;
        }

        updateBuff();
    }

    public void updateBuff()//Da li da se skine buff ili ne
    {
        if (buffed != 0)
        {
            if (--buffed == 0)                
                if (getType() == "Tank")
                    tankBonus = 20;
                else
                    dpsBonus = 20;      
        }
    }

    public void SetBuffed(int n)
    {
        buffed = 2;
    }

    public void stunZaramin()//Stunuj Zaramina
    {
        stunned = true;
    }

    public void unstunZaramin()//Povadi ga iz stun
    {
        stunned = false;
    }

    public bool isStunned()//Da li je stunovan
    {
        return stunned;
    }

    public void Stun(Zaramin z)
    {
        cooldowns[1] += magics[1].getCooldown() + 1;
        currentMana -= magics[1].getMana();
        z.stunZaramin();
    }

    public int attack(Magic m, Zaramin z)//Svaka Rasa Zaramina implementira ponaosob
    {
        cooldowns[getMagicIndex(m)] += m.getCooldown() + 1;//Postavi cooldown

        currentMana -= m.getMana();

        if (z.getRace() == getOpposite())//Provera da li je "suprotan" element
            return z.takeDamage((int)((m.doMagic() + level) * (dpsBonus/100f + 1.1f)));//Bonus dmg
        else
            return z.takeDamage((int)((m.doMagic() + level) * (dpsBonus / 100f + 1f)));//Ili ne
    }

    public void setExperience(int newExperience)//Dodaj exp
    {
        experience = newExperience;
    }

    
    public void changeExperience(int newValue)//Promeni exp
    {
        experience += newValue;
        calculateLevel();//Preracunaj exp
    }

    public void getMagicIndices(Magic [] magicArray)//Preuzimanje magija, usmeravanje pointera
    {
        magics[0] = magicArray[0];//Prva je uvek toljaga
        if (healerBonus != 0)//Healer magije
        {
            magics[1] = magicArray[1];
            magics[2] = magicArray[2];
        }
        else
            if (dpsBonus != 0)//DPS magije
            {
                magics[1] = magicArray[3];
                magics[2] = magicArray[4];
            }
            else//Tank magije
            {
                magics[1] = magicArray[5];
                magics[2] = magicArray[6];
            }
    }

   

    public void resetCooldown()//Resetuje cooldown-e
    {
        for (int i = 0; i < 3; i++)
            cooldowns[i] = 0;
    }

    public void setCooldown(int index, int value)//Postavi cooldown odredjenoj magiji
    {
        cooldowns[index] += value + 1;
    }
        

    public bool Load(string id) //OVO BI TREBALO PREPRAVITI!
    {
        using (StreamReader f = new StreamReader(id))
        {
            string health = f.ReadLine();
            string mana = f.ReadLine();

            maxHealth = currentHealth = Convert.ToInt32(health);
            maxMana = currentMana = Convert.ToInt32(mana);
            
            return true;
        }
        
    }

    public void calculateLevel()//Izracunava level na osnovu exp
    {
        level = experience / 1000 + 1;
    }

    public bool Write(string id)//NEPOTREBNO
    {
        throw new NotImplementedException();
    }
    
    public void usePotion(string pot)
    {
        if (pot == "Mana")
        {
            currentMana += 100;
            if (currentMana > maxMana)
                currentMana = maxMana;
        }
        else
        {
            currentHealth += 100;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
        }


    }
    public void getTexture()//Nabavka Sprite-ova
    {
        string type = (dpsBonus != 0) ? "DPS" : (healerBonus != 0) ? "Healer" : "Tank";
        string maxi = getRace() + type + "Maxi";
        string mini = getRace() + type + "Mini";

        int i = 0;

        Sprite[] niz = Resources.LoadAll<Sprite>("ZaraminsFinal/ZaraminsMaxi");

        while (niz[i++].name != maxi) ;

        main = niz[--i];

        i = 0;

        niz = Resources.LoadAll<Sprite>("ZaraminsFinal/MagicMini");

        while (niz[i++].name != mini) ;
        small = niz[--i];
    }

    //Setteri
    public void setMaxHealth()//Povecava health ako je na svom polju, i menja current health takodje
    {
        maxHealth = (int)(maxHealth * 1.1f);
        currentHealth = maxHealth;
    }
    //Getteri

    public Magic[] getMagics()//Vraca niz magija
    {
        return magics;
    }

    public int getCurrentHealth()//Vraca trenutni HP
    {
        return this.currentHealth;
    }

    public int getMaxHealth()//Vraca maksimalni HP
    {
        return maxHealth;
    }

    public int[] getCooldowns()
    {
        return cooldowns;
    }


    public virtual string getRace() { return "fail"; }//Ovo nikad ne bi trebalo da se pozove

    public int getExperience()//Getter za exp
    {
        return experience;
    }

    public string getName()//Getter za ime
    {
        return name;
    }

    public int getCurrentMana()//Vraca trenutnu manu
    {
        return currentMana;
    }

    public int getMaxMana()//Vraca maksimalnu manu
    {
        return maxMana;
    }


    public void DoSelf()//Odradi self magiju
    {
        if (healerBonus != 0)

            takeDamage(magics[2].getDamage());
        else
            if (dpsBonus != 0)

                dpsBonus += magics[2].getDamage();
            else
                tankBonus += magics[2].getDamage();

        cooldowns[2] += magics[2].getCooldown() + 1;

        currentMana -= magics[2].getMana();
    }


    public string getInfo()//Infobox
    {
        return "Name: " + name + "\nRace: " + getRace() + "\nLevel: " + level + "\nHealth: " + currentHealth + "\nMana: " + currentMana;
    }

    public string getCardInfo()
    {
        return "Level: " + level + "\nSpec: " + getType() + "\nExp: " + experience;
    }


    public bool isDead()
    {
        return (currentHealth == 0) ? true : false;
    }

    public virtual string getOpposite()
    {
        return "";
    }

}


public class FireZaramin : Zaramin
{
	//Konstruktor za Fire Zaramina
	public FireZaramin (string name,int dps,int healer,int tank,int experience, Magic[] magics) 
                       : base (name,dps,healer,tank,experience)
    {        
		getMagicIndices(magics);//Puni odgovarajucim magijama
        getTexture();//Nalazi odgovarajuce Sprite-ove
	}

    public override string getOpposite()
    {
        return "Dark";
    }

    public override string getRace()//Getter za rasu
    {
        return "Fire";
    }	
}


public class WaterZaramin : Zaramin
{
	//Konstruktor za Water Zaramina
	public WaterZaramin (string name,int dps,int healer,int tank,int experience, Magic[] magics) 
                        : base (name,dps,healer,tank,experience)
    {        
		getMagicIndices(magics);//Puni odgovarajucim magijama
        getTexture();//Nalazi odgovarajuce Sprite-ove
	}

    public override string getOpposite()
    {
        return "Fire";
    }

    public override string getRace()//Getter za rasu
    {
        return "Water";
    }
}


public class EarthZaramin : Zaramin
{
    //Konstruktor za Water Zaramina
	public EarthZaramin (string name,int dps,int healer,int tank,int experience, Magic[] magics) 
                        : base (name,dps,healer,tank,experience)
    {        
		getMagicIndices(magics);//Puni odgovarajucim magijama
        getTexture();//Nalazi odgovarajuce Sprite-ove
	}

    public override string getOpposite()
    {
        return "Water";
    }

    public override string getRace()//Getter za rasu
    {
        return "Earth";
    }
}


public class DarkZaramin : Zaramin
{
	//Konstruktor za Water Zaramina
    public DarkZaramin(string name, int dps, int healer, int tank, int experience, Magic[] magics) 
                        : base (name,dps,healer,tank,experience)
    {        
		getMagicIndices(magics);//Puni odgovarajucim magijama
        getTexture();//Nalazi odgovarajuce Sprite-ove
	}

    public override string getOpposite()
    {
        return "Earth";
    }

    public override string getRace()//Getter za rasu
    {
        return "Dark";
    }
}


public interface IFile//Interface za upis
{
    bool Load(string id);
    bool Write(string id);
}



