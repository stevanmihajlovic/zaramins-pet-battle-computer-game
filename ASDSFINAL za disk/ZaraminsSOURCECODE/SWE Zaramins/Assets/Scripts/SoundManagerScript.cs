﻿using UnityEngine;
using System.Collections;

public class SoundManagerScript : MonoBehaviour {

    public AudioSource source;
    public AudioClip ButtonWood;
    public AudioClip ButtonRock;
    public AudioClip InGameSoundtrack;
    public AudioClip InBattleSoundtrack;
    public AudioClip PrepareForBattle;

	// Use this for initialization
	void Start () {

        source = GameObject.Find("SoundManager").GetComponent<AudioSource>();
        source.volume = 1;
        source.clip = InGameSoundtrack;
        source.loop = true;
        source.Play();
	}
    void Awake ()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void onClickSound ()
    {
        source.PlayOneShot(ButtonWood);
    }
    public void BattleBegin ()
    {
        source.Stop();
        source.clip = InBattleSoundtrack;
        source.loop = true;
        source.Play();

    }
    public void BattleEnds ()
    {
        source.Stop();
        source.clip = InGameSoundtrack;
        source.loop = true;
        source.Play();
    }
    public void PrepareBattle()
    {
        source.PlayOneShot(PrepareForBattle);
    }
    public void RockSound()
    {
        source.PlayOneShot(ButtonRock);
    }
    public void StopSound()
    {
        source.Stop();
    }
}
