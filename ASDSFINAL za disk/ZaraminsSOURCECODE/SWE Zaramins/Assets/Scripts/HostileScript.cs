﻿using UnityEngine;
using System.Collections;

public class HostileScript : MonoBehaviour {

    public GameControlScript gameController;
	private int number = 0;
    private TextMesh infoText;
    public SoundManagerScript script;

	// Use this for initialization
	void Start () 
    {
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	// Update is called once per frame
	void Update () 
	{
	
	}

    void OnMouseUpAsButton()
    {
        if (gameController.phase == 2)
		{
            script.onClickSound();
			if (this.name == "Zaramin4")
				number = 0;
			else
				if (this.name == "Zaramin5")
					number = 1;
			    else
				    number = 2;
            gameController.phase23(number);
		}
        else
        {
            /*kod za info, mozda*/
        }
    }


    void OnMouseOver()
    {
        if (name == "Zaramin4")
        {
            infoText.text = gameController.enemyZaramins[0].getInfo();
        }
        else if (name == "Zaramin5")
        {
            infoText.text = gameController.enemyZaramins[1].getInfo();
        }
        else
        {
            infoText.text = gameController.enemyZaramins[2].getInfo();
        }
    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
