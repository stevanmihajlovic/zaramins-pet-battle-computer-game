﻿using UnityEngine;
using System.Collections;

public class LanMaskScript : MonoBehaviour
{

    public LanGameControlScript gameController;

    // Use this for initialization
    void Start()
    {
        gameController = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseUpAsButton()
    {
        if (gameController.phase == 1)
            gameController.phase10();
        else
            if (gameController.phase == 2)
                gameController.phase21();
    }
}