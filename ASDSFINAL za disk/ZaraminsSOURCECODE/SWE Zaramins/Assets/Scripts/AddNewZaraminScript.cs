﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class AddNewZaraminScript : MonoBehaviour
{
    static GameObject firstGlow, secondGlow;//Gameobject sa Glow-om
    GameObject inputZaraminName;//Input field
    static string race, spec;//Nova rasa i specijalizacija
    static bool raceF, specializationF;//Flegovi da li su rasa i specijalizacija odabrani
    private InfoBoxScript infoBox;
    public SoundManagerScript script;


	void Start ()
    {
        raceF = specializationF = false;
        firstGlow = GameObject.Find("AddZaraminPanel/Fire/SelectedGlow");//Svi uzimaju referencu na prvi glow
        secondGlow = GameObject.Find("AddZaraminPanel/Healer/SelectedGlow");//Svi uzimaju referencu na drugi glow
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();       
    }


    void Update()
    {}


    void OnMouseUpAsButton()//Provera koje dugme je kliknuto
    {
        script.RockSound();
        if (name == "Fire" || name == "Earth" ||name == "Water" ||name == "Dark")//Odabir Rase
        {
            firstGlow.SetActive(false);//Gasi prvi glow
            firstGlow = GameObject.Find("AddZaraminPanel/" + name + "/SelectedGlow");//Promena prvog glow-a
            firstGlow.SetActive(true);//Pali prvi glow
            race = name;//Nova rasa
            raceF = true;//Rasa odabrana
        }


        if (name == "Healer" || name == "Tank" || name == "DPS")//Odabir Specijalizacije
        {
            secondGlow.SetActive(false);//Gasi drugi glow
            secondGlow = GameObject.Find("AddZaraminPanel/" + name + "/SelectedGlow");//Promena drugog glow-a
            secondGlow.SetActive(true);//Pali drugi glow
            if (name == "Healer")//definisanje bonusa u zavisnosti od odabira
                spec = "Healer";
            else
                if (name == "Tank")
                    spec = "Tank";
                else
                spec = "DPS";

            specializationF = true;//Specijalizacija odabrana
        }

        if (name == "Done")
        {
            //Provera da li su odabrani svi potrebni atributi
            if (raceF && specializationF && GameObject.Find("Canvas/InputField/Text").GetComponent<Text>().text != "")
            {
                if (GameObject.Find("Canvas/InputField/Text").GetComponent<Text>().text.Length > 12)
                    infoBox.Show("Too long!", "Name can contain only\ntwelve (12) characters!");
                else
                {
                    GameObject.Find("MyZaraminsController").GetComponent<MyZaraminsController>().AddZaramin(race, spec,
                    GameObject.Find("Canvas/InputField/Text").GetComponent<Text>().text);//Dodavanje Zaramina igracu
                    onExit();//Resetovanje panela
                }
            }
            else
                infoBox.Show("Missing!", "You haven't chosen\nall the necessary atributes!");               
        }

        if (name == "Exit")//Resetovanje panela
            onExit();
    }

    void onExit()//Resetovanje panela
    {
        GameObject.Find("Canvas/InputField").GetComponent<InputField>().text = "";//Izbrisi InputField
        GameObject.Find("AddZaraminPanel").transform.position = new Vector3(20, 0, -3.5f);//Pomeri panel
        GameObject.Find("Canvas/InputField").transform.localScale = new Vector3(0, 0, 0);//Smanji InputField
        firstGlow.SetActive(false);//Gasi prvi glow
        secondGlow.SetActive(false);//Gasi drugi glow
        specializationF = false;//Resetuj flegove za rasu i specijalizaciju
        raceF = false;
    }

    

}
