﻿using UnityEngine;
using System.Collections;

public class LanFriendlyScript : MonoBehaviour
{
    public LanGameControlScript lanGameController;
    private float drawerOffset = 2.65f;
    private int number = 0;
    private TextMesh infoText;
    public SoundManagerScript script;
    // Use this for initialization
    void Start()
    {
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        lanGameController = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseUpAsButton()
    {
        if (lanGameController.phase == 0 || lanGameController.phase == 1)
        {
            script.onClickSound();
            if (name == "Zaramin1")
                number = 0;
            else
                if (name == "Zaramin2")
                    number = 1;
                else
                    number = 2;

            lanGameController.phase01(new Vector3(transform.position.x + drawerOffset,
                                               transform.position.y, -2), number);
        }
        else
        {
            
            if (lanGameController.phase == 2)
            {
                script.onClickSound();
                if (name == "Zaramin1")
                    number = 0;
                else
                    if (name == "Zaramin2")
                        number = 1;
                    else
                        number = 2;
                lanGameController.phase23(number);
            }
        }
    }

    void OnMouseOver()
    {
        if (name == "Zaramin1")
        {
            infoText.text = lanGameController.player.myZaramins[lanGameController.player.activeZaramins[0]].getInfo();
        }
        else if (name == "Zaramin2")
        {
            infoText.text = lanGameController.player.myZaramins[lanGameController.player.activeZaramins[1]].getInfo();
        }
        else
        {
            infoText.text = lanGameController.player.myZaramins[lanGameController.player.activeZaramins[2]].getInfo();
        }
    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
