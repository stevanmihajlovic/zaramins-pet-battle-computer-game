﻿using UnityEngine;
using System.Collections;

public class MyZaraminsController : MonoBehaviour
{
    private PlayerScript player;//Igrac
    private Zaramin selectedZaramin;//Odabran Zaramin //MOZDA NE TREBA NA KRAJU
    public GameObject[] solo, team;//Niz Zaramin ikonica solo i timski
    private SpriteRenderer frame, bigPicture;//Karta
    public Sprite fire, earth, water, dark, glow;//Ramovi
    private int selectedZaraminNumber;//Redni broj u myZaramins nizu //VEROVATNO POTREBNO ZA BRISANJE
    public  TextMesh zaraminName,zaraminInfo;//Podaci o Zaraminu
    public GameObject addToTeam, removeFromTeam;
    private InfoBoxScript infoBox;

	
	void Start ()
    {
        
        GameObject.Find("Canvas/InputField").transform.localScale = new Vector3(0, 0, 0);//Skrivam InputField
        player = GameObject.Find("Player").GetComponent<PlayerScript>();//Igrac
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        frame = GameObject.Find("Card/CardFrame").GetComponent<SpriteRenderer>();//Ram
        bigPicture = GameObject.Find("Card/ZaraminImage").GetComponent<SpriteRenderer>();//Slika rama
        if (player.getZaraminsCount() > 0)//Prvi prikaz!
            showCard(0);
        else
            selectedZaraminNumber = -1;
        Paint();//Slika scenu

	}
	

	void Update ()
    {}

    public void selectZaraminChangeCard(int number, string type)//Selektovanje Zaramina, promena karte i add/rem buttona
    {
        if (type == "team")
        {
            selectedZaraminNumber = player.activeZaramins[number];
            selectedZaramin = player.myZaramins[player.activeZaramins[number]];
            RemInFront();
        }
        else
        {
            selectedZaraminNumber = number;
            selectedZaramin = player.myZaramins[number];
            if (player.InTeam(number))
                RemInFront();
            else
                AddInFront();
        }

        bigPicture.sprite = selectedZaramin.main;
        FrameChange(selectedZaramin.getRace());
        zaraminName.text = selectedZaramin.getName();
        zaraminInfo.text = selectedZaramin.getCardInfo();
    }


    public void Paint()//Slika scenu
    {
        int i = 0;

        while (i < player.getZaraminsCount())
        {
            solo[i].transform.localScale = new Vector3(1, 1, 1);
            solo[i].GetComponent<SpriteRenderer>().sprite = player.myZaramins[i++].small;
        }

        i = 0;

        while (i < 3)//AKO SE SETIM CEMU OVO player.getZaraminsCount() > 2 &&
            if (player.activeZaramins[i] > -1)
            {
                team[i].transform.localScale = new Vector3(1, 1, 1);
                team[i].GetComponent<SpriteRenderer>().sprite = player.myZaramins[player.activeZaramins[i++]].small;
            }
            else
            {
                team[i].transform.localScale = new Vector3(0.67f, 0.67f, 1);
                team[i++].GetComponent<SpriteRenderer>().sprite = glow;
            }

    }


    private void FrameChange(string race)//Za promenu rama
    {
        if (race == "Fire")
            frame.sprite = fire;
        else
            if (race == "Dark")
                frame.sprite = dark;
            else
                if (race == "Earth")
                    frame.sprite = earth;
                else
                    frame.sprite = water;
    }

    public void AddZaramin(string race, string spec, string zName)//Dodaj Zaramina igracu
    {
        player.myZaramins[player.getZaraminsCount()] = player.getZaramin(zName, race, spec, 0, player.getMagicPanel());
        player.incrementZaraminsCount();
        showCard(player.getZaraminsCount() - 1);
        Paint();
    }

    public void AddToTeam()//Dodavanje selektovanog Zaramina u tim
    {
        if (selectedZaraminNumber != -1)
        {
            if (player.TeamFull())//Provera da li ima mesta u timu
                infoBox.Show("Team full!", "Remove a Zaramin\nfrom team first!");
            else
            {
                player.TeamJoin(selectedZaraminNumber);//Dodavanje Zaramina u tim
                Paint();//Repaint
                RemInFront();//Zamena buttona
            }
        }
        else
            infoBox.Show("Select first!", "You haven't selected\na Zaramin!");
    }

    public void RemoveFromTeam()//Izbacivanje selektovanog Zaramina iz tima
    {
        if (selectedZaraminNumber != -1)
        {
            player.TeamRemove(selectedZaraminNumber);//Izbacivanje Zaramina iz tima
            Paint();//Repaint
            AddInFront();//Zamena buttona
        }
        else
            infoBox.Show("Select first!", "You haven't selected\na Zaramin!");

    }

    public void DeleteZaramin()//Brisanje Zaramina
    {
        if (selectedZaraminNumber != -1)
        {
            player.DeleteZaramin(selectedZaraminNumber);//Brisanje Zaramina
            GameObject.Find("Zaramins/Zaramin" + (player.getZaraminsCount() + 1)).transform.localScale = new Vector3(0.67f, 0.67f, 1);
            GameObject.Find("Zaramins/Zaramin" + (player.getZaraminsCount() + 1)).GetComponent<SpriteRenderer>().sprite = glow;
            GameObject.Find("Zaramins/Zaramin" + (selectedZaraminNumber + 1) + "/SelectedGlow").SetActive(false);

            if (player.getZaraminsCount() > 0)
            {
                Paint();
                showCard(0);
            }
            else
            {
                selectedZaraminNumber = -1;
                selectedZaramin = null;
                Paint();
                AddInFront();
            }
        }
        else
            infoBox.Show("Select first!", "You haven't selected\na Zaramin!");
    }

    private void AddInFront()//AddZaraminToTeam button napred
    {
        addToTeam.transform.position = new Vector3(addToTeam.transform.position.x, addToTeam.transform.position.y, -1);
        removeFromTeam.transform.position = new Vector3(removeFromTeam.transform.position.x, removeFromTeam.transform.position.y, 1);
    }

    private void RemInFront()//RemZaraminfromTeam button napred
    {
        addToTeam.transform.position = new Vector3(addToTeam.transform.position.x, addToTeam.transform.position.y, 1);
        removeFromTeam.transform.position = new Vector3(removeFromTeam.transform.position.x, removeFromTeam.transform.position.y, -1);
    }

    public void showCard(int i)
    {
        selectedZaraminNumber = i;
        selectedZaramin = player.myZaramins[selectedZaraminNumber];

        FrameChange(selectedZaramin.getRace());        
        bigPicture.sprite = selectedZaramin.main;
        zaraminName.text = selectedZaramin.getName();
        zaraminInfo.text = selectedZaramin.getCardInfo();

        if (player.InTeam(selectedZaraminNumber))
            RemInFront();
        else
            AddInFront();

        GameObject.Find("Zaramins/Zaramin" + (selectedZaraminNumber + 1)).GetComponent<MyZaraminsGlow>().ChangeGlow();
    }
}
