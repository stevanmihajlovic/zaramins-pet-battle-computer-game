﻿using UnityEngine;
using System.Collections;

public class RemoveStatsScript : MonoBehaviour
{
    GameObject box;
    SoundManagerScript script;
    PlayerScript player;

    void Start()
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
        box = GameObject.Find("Statistics");
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
    }

    void Update()
    {}

    void OnMouseUpAsButton()//Klik
    {
        script.onClickSound();
        box.transform.position = new Vector3(-20, 0, -2);
        player.Write("");
    }
}
