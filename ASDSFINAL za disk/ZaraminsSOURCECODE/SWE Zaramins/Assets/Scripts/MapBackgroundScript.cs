﻿using UnityEngine;
using System.Collections;

public class MapBackgroundScript : MonoBehaviour {

    public LevelSceneControllerScript skripta;
    public LevelScript levelScript;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseUpAsButton ()
    {
        skripta.unselectAllLevels();
        skripta.selectedLevel = -1;
        levelScript.bossName.text = "";
        levelScript.bossInfo.text = "";
        
    }
}
