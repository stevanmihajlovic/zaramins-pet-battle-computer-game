﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CreateAccAndLogInScript : MonoBehaviour
{

    //Koristi se za tabovanje i kraj unosa podataka (enter, return)
	EventSystem system;
    //Objekat koji sadrzi sve magije (i sve vezano za njih) i mesto za player name
    private PlayerScript player;
    //Sprite za on i off stanje
    public Sprite on, off;
    //Za infobox za greske
    private InfoBoxScript infoBox;
    public SoundManagerScript script;


	void Start ()
	{
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
		system = EventSystem.current;
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
	}


    public void Update()//Kod za tabovanje
	{

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			if (next!= null) 
			{			
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(system));
				system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
			}
		}
		if (Input.GetKeyDown("enter") || Input.GetKeyDown("return"))
            OnMouseUpAsButton();
	}


    void OnMouseUpAsButton()//Kod za logovanje
    {       
        string username;
        string password;
        string retype;
        username = GameObject.Find("Canvas/Username/Text").GetComponent<Text>().text;
        password = GameObject.Find("Canvas/Password").GetComponent<InputField>().text;
        retype = GameObject.Find("Canvas/PasswordRetype").GetComponent<InputField>().text;
        script.onClickSound();
        if (!PlayerPrefs.HasKey(username) && password != "" && username != "" )
        {
            if (password == retype)
            {
                player.PlayerScriptConstructor(username);
                PlayerPrefs.SetString(username, password);
                Application.LoadLevel(2);
            }
            else
                infoBox.Show("Pass missmatch!", "Please reenter both\npasswords again!");
        }
        else
            infoBox.Show("Username exists!", "That username is\nalready in use.\nPlease reenter a new one!");
    }

    void OnMouseEnter()//Pocetak hovera
    {
        GetComponent<SpriteRenderer>().sprite = on;
        gameObject.transform.localScale = new Vector3(2, 2, 1);
    }

    void OnMouseExit()//Kraj hovera
    {
        GetComponent<SpriteRenderer>().sprite = off;
        gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

}
