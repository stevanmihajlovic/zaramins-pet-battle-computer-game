﻿using UnityEngine;
using System.Collections;

public class FriendlyScript : MonoBehaviour {

    public GameControlScript gameController;
    private float drawerOffset = 2.65f;
	private int number = 0;
    private TextMesh infoText;
    public SoundManagerScript script;
	// Use this for initialization
    void Start()
    {
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()
    {
        if (gameController.phase == 0 || gameController.phase == 1)
		{
            script.onClickSound();
            if (name == "Zaramin1")
				number = 0;
			else
				if (name == "Zaramin2")
					number = 1;
				else
					number = 2;

			gameController.phase01(new Vector3(transform.position.x + drawerOffset,
			                                   transform.position.y, -2), number);
		}
		else
        {
            if (gameController.phase == 2)
            {
                script.onClickSound();
                if (name == "Zaramin1")
                    number = 0;
                else
                    if (name == "Zaramin2")
                        number = 1;
                    else
                        number = 2;
                gameController.phase23(number);
            }
        }
    }

    void OnMouseOver()
    {
        if (name == "Zaramin1")
        {
            infoText.text = gameController.player.myZaramins[gameController.player.activeZaramins[0]].getInfo();
        }
        else if (name == "Zaramin2")
        {
            infoText.text = gameController.player.myZaramins[gameController.player.activeZaramins[1]].getInfo();
        }
        else
        {
            infoText.text = gameController.player.myZaramins[gameController.player.activeZaramins[2]].getInfo();
        }
    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
