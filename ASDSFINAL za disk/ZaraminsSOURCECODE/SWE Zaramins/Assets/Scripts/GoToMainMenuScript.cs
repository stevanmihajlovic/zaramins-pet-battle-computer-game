﻿using UnityEngine;
using System.Collections;


public class GoToMainMenuScript : MonoBehaviour
{
    public SoundManagerScript script;

	void Start ()
    {
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }
	
	void Update ()
    {}

    void OnMouseUpAsButton()
	{
        script.onClickSound();
        GameObject.Find("Player").GetComponent<PlayerScript>().Write("");
        Application.LoadLevel(2);
	}
}
