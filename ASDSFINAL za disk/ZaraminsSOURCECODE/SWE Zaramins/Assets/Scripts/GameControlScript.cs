﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;

public class GameControlScript : MonoBehaviour {

    private int level, zaraminForUpdate;
    public int phase;
    private string bossName, potion;
    private bool potionUsed = false;

    public Zaramin firstZaraminClicked, secondZaraminClicked;
    public Magic magicClicked;//---->menja vrednost tokom borbe

    public PlayerScript player;
    public Zaramin[] enemyZaramins;
    private MagicPanelScript magicPanel;

    private GameObject friendlyZaramins, hostileZaramins;//Grupe Zaramina

    private GameObject playerTimerOpponent, mask, drawer, healthFill, manaFill;

    private TimerScript Timer;

    private SpriteRenderer firstMagic, secondMagic, thirdMagic;

    private float length;

    private BackgroundScript background;

    private InfoBoxScript infoBox;

    public Text log;

    private Animation yourturn, opponentsturn;
    public SoundManagerScript script;
    
    

	// Use this for initialization
	void Start ()
	{
        phase = 0;
        firstMagic = GameObject.Find("Drawer/FirstAttack").GetComponent<SpriteRenderer>();
        secondMagic = GameObject.Find("Drawer/SecondAttack").GetComponent<SpriteRenderer>();
        thirdMagic = GameObject.Find("Drawer/ThirdAttack").GetComponent<SpriteRenderer>();

        yourturn = GameObject.Find("YourTurn").GetComponent<Animation>();
        opponentsturn = GameObject.Find("OpponentTurn").GetComponent<Animation>();

        log = GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>();
        
        infoBox = GameObject.Find("InfoBox").GetComponent<InfoBoxScript>();
        mask = GameObject.Find("Mask");
        drawer = GameObject.Find("Drawer");
        friendlyZaramins = GameObject.Find("FriendlyZaramins");
        hostileZaramins = GameObject.Find("HostileZaramins");
        healthFill = GameObject.Find("Zaramin1/HPMana/Health/TimeBarFill");
       
        Timer = GameObject.Find("PTO/Timer").GetComponent<TimerScript>();
        Timer.control = this;        
        
        magicPanel = GameObject.Find("MagicPanel").GetComponent<MagicPanelScript>();

        player = GameObject.Find("Player").GetComponent<PlayerScript>();

        background = GameObject.Find("Background").GetComponent<BackgroundScript>();

        chooseBackground();

        length = healthFill.transform.localScale.x;

        loadLevel(player.currentLevel);

        GameObject.Find("PTO/Player/PlayerText").GetComponent<TextMesh>().text = player.getName() + "\nLevel: " + player.getLevel();
        GameObject.Find("PTO/Opponent/OpponentText").GetComponent<TextMesh>().text = bossName + "\nLevel: 20";
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
        script.BattleBegin();
        Paint();//Crta Zaramine
        friendlyForward();//Nasi napred
        prepareZaramins();
        yourturn.Play();
        Timer.StartTimer();//Pokretanje timera

	}


    void Update()
    {}

 
    public void phase01(Vector3 vector, int number)//Odaberi Zaramina ILI POTION
    {
        potionUsed = false;
        firstZaraminClicked = player.myZaramins[player.activeZaramins[number]];//Pamtimo prvog kliknutog Zaramina

        if (!firstZaraminClicked.isStunned() && !firstZaraminClicked.isDead())
        {
            drawer.transform.position = vector;//Pomeranje drawera

            drawDrawer();//Crtanje magija

            mask.transform.position = new Vector3(0, 0, 0);//Pomeranje maske

            phase = 1;

            zaraminForUpdate = number;//Ciji HP i mana bar menjamo na kraju

        }
        else
            if (firstZaraminClicked.isStunned())
                infoBox.Show("Stunned!", "That Zaramin is\nstunned!");
            else
                infoBox.Show("Dead!", "That Zaramin is\ndead!");
    }

    public void phase10()//Povratak u prethodno stanje
    {
        drawer.transform.position = new Vector3(20, 0);//Sklanjamo drawer

        phase = 0;
        
        mask.transform.position = new Vector3(20, 0);//Sklanjamo masku
    }
     
    public void phase12(int number)
    {
		magicClicked = firstZaraminClicked.getMagics()[number];//Pamtimo izabranu magiju
        potionUsed = false;
        //Provera da li je magiju moguce iskoristiti
        if (firstZaraminClicked.getCooldowns()[number] == 0 && firstZaraminClicked.getCurrentMana() >= magicClicked.getMana())
        {
            if (magicClicked.getFriendly())//Da li je magija prijateljska
            {
                if (magicClicked.getSelf())//Da li je self-only
                {
                    if (firstZaraminClicked.getType() != "Healer")

                        firstZaraminClicked.SetBuffed(2);

                    firstZaraminClicked.DoSelf();//Odradi self magiju

                    log.text += "\n>>" + firstZaraminClicked.getName() + " used " + magicClicked.getName() + " on itself" +
                    ("Healer" == firstZaraminClicked.getType() ? (". Healed itself for " + -magicClicked.getDamage() + " points!") : "!");

                    phase34();//Kraj poteza

                    return;
                }

                friendlyForward();
            }

            else
                hostileForward();//Menjamo mesta Zaraminima

            drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, 2);

            phase = 2;
        }
        else
            if (firstZaraminClicked.getCooldowns()[number] != 0)
                infoBox.Show("Cooldown!", "That ability is\non cooldown!");
            else
                infoBox.Show("Mana low!", "That ability requires\nmore mana!");
    }

    public void phase21()
    {
        magicClicked = null;
        drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, -2);
        friendlyForward();
        phase = 1;
    }

    public void phasex2(string pot)
    {
        if ((pot == "Mana" && player.getManaPot() > 0) || (pot == "Health" && player.getHealthPot() > 0))
        {
            mask.transform.position = new Vector3(0, 0);
            friendlyForward();
            phase = 2;
            potionUsed = true;
            firstZaraminClicked = null;
            zaraminForUpdate = -1;
            potion = pot;
            drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        }
        else
            infoBox.Show("No potions!", "You have no potions\nof that type!\nBuy some at Statistics!");
    }

    public void phase23(int number)
    {
        int dmg;

        if (potionUsed)
        {
            secondZaraminClicked = player.myZaramins[player.activeZaramins[number]];

            if (!secondZaraminClicked.isDead())
            {
                if (potion == "Mana")
                    player.decManaPot();
                else
                    player.decHealthPot();
                zaraminForUpdate = number;
                secondZaraminClicked.usePotion(potion);
                log.text += "\n>>" + player.getName() + " used " + potion + " potion on " + secondZaraminClicked.getName() + " and it recovered 100 points of "
                    + potion + "!";
            }
            else
            {
                infoBox.Show("Dead!", "That Zaramin is\ndead!");
                return;
            }
        }
        else
        {
            if (magicClicked.getFriendly())
            {
                secondZaraminClicked = player.myZaramins[player.activeZaramins[number]];

                if (!secondZaraminClicked.isDead())
                {
                    dmg = firstZaraminClicked.attack(magicClicked, secondZaraminClicked);
                    UpdateHPMana(number);
                    log.text += "\n>>" + firstZaraminClicked.getName() + " healed " + secondZaraminClicked.getName() + " with " +
                    magicClicked.getName() + " for " + -dmg + " points!";
                }
                else
                {
                    infoBox.Show("Dead!", "That Zaramin is\ndead!");
                    return;
                }
            }
            else
            {
                secondZaraminClicked = enemyZaramins[number];

                if (!secondZaraminClicked.isDead())
                {
                    if (magicClicked.getStun())
                    {
                        firstZaraminClicked.Stun(secondZaraminClicked);
                        log.text += "\n>>" + firstZaraminClicked.getName() + " stunned " + secondZaraminClicked.getName() + " for one turn!";
                    }
                    else
                    {
                        dmg = firstZaraminClicked.attack(magicClicked, secondZaraminClicked);
                        UpdateHPMana(3 + number);
                        log.text += "\n>>" + firstZaraminClicked.getName() + " attacked " + secondZaraminClicked.getName() + " with " +
                        magicClicked.getName() + " for " + dmg + " points!";
                    }
                }
                else
                {
                    infoBox.Show("Dead!", "That Zaramin is\ndead!");
                    return;
                }
            }
        }

        friendlyForward();
        //drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        //mask.transform.position = new Vector3(20, 0);

        phase = 3;
                
        phase34();
        
    }

    public void phase34 ()
    {
        UpdateHPMana(zaraminForUpdate);//Zbog Self magija
        mask.transform.position = new Vector3(20, 0);
        drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        phase = 4;
        if (checkPlayerWin())
            winGame();
        else
        {
            opponentsturn.Play();
            Timer.ResetTimer();
        }
        
    }

    public void loadLevel (int levelNo)
    {
        using (StreamReader s = new StreamReader(Application.dataPath + "/Levels/" + levelNo.ToString()+".txt"))
        {
            
            string type = s.ReadLine();
            bossName = s.ReadLine();
            if (type == "normal")
            {
                int count = Convert.ToInt32(s.ReadLine());
                enemyZaramins = new Zaramin[count];
                s.ReadLine();


                for (int i = 0; i<count; i++)
                {
                    string name = s.ReadLine();
                    string exp = s.ReadLine();
                    string typeZ = s.ReadLine();
                    string dht = s.ReadLine();
                    s.ReadLine();

                    enemyZaramins[i] = player.getZaramin(name,typeZ,dht,Convert.ToInt32(exp),magicPanel);
                }
            }
            else
            {
                //ucitavanje specijalnog nivoa sa Boss-om.
            }
        }
    }
    public void enemyPlay ()
    {
        int dmg, n, m, o;

        n = m = o = 0;

        firstZaraminClicked = null;

        secondZaraminClicked = null;

        magicClicked = null;

        for (int i=0; i<3; i++)
            {
                player.myZaramins[player.activeZaramins[i]].unstunZaramin();
                enemyZaramins[i].updateZaramin();
            }

        if (!onlyAliveStunned())
        {
            while (firstZaraminClicked == null)
            {
                n = (int)UnityEngine.Random.RandomRange(0f, 2.99f);

                if (!enemyZaramins[n].isDead() && !enemyZaramins[n].isStunned())
                    firstZaraminClicked = enemyZaramins[n];
            }

            while (secondZaraminClicked == null)
            {
                m = (int)UnityEngine.Random.RandomRange(0f, 2.99f);

                if (!player.myZaramins[player.activeZaramins[m]].isDead())
                    secondZaraminClicked = player.myZaramins[player.activeZaramins[m]];
            }

            while (magicClicked == null)
            {
                if (firstZaraminClicked.getType() != "Healer")
                    o = (int)UnityEngine.Random.RandomRange(0f, 1.99f);
                else
                    o = 0;
                if (firstZaraminClicked.getCooldowns()[o] == 0)
                    magicClicked = firstZaraminClicked.getMagics()[o];
            }

            if (magicClicked.getStun())
            {
                firstZaraminClicked.Stun(secondZaraminClicked);
                log.text += "\n>>" + firstZaraminClicked.getName() + " stunned " + secondZaraminClicked.getName() + " for one turn!";
            }
            else
            {
                dmg = firstZaraminClicked.attack(magicClicked, secondZaraminClicked);
                log.text += "\n>>" + firstZaraminClicked.getName() + " attacked " + secondZaraminClicked.getName() +
                    " for " + dmg + " points!";
            }
        }


        UpdateHPMana(m);
        UpdateHPMana(n+3);
                
        
        
        for (int i = 0; i < 3; i++)
        {
            player.myZaramins[player.activeZaramins[i]].updateZaramin();
            enemyZaramins[i].unstunZaramin();
        }

        if (checkComputerWin())
            loseGame();
        else
        {
            phase = 0;
            Timer.ResetTimer();
            yourturn.Play();
        }
        
    }
    
    public void changePlayer ()
    {
        if (phase < 3)
        {
            phase = 3;
            phase34();
        }
        else
        {
            phase = 0;
        }
    }

    public void UpdateHPMana (int zaraminN)
    {
        healthFill = GameObject.Find("Zaramin" + zaraminN + "/HPMana/Health/TimeBarFill");
        manaFill = GameObject.Find("Zaramin" + zaraminN + "/HPMana/Health/TimeBarFill");

        if (zaraminN<3)//Ako je zaramin od player-a
        {
            healthFill = GameObject.Find("FriendlyZaramins/Zaramin" + (zaraminN+1) + "/HPMana/Health/TimeBarFill");
            manaFill = GameObject.Find("FriendlyZaramins/Zaramin" + (zaraminN+1) + "/HPMana/Mana/TimeBarFill");
            healthFill.transform.localScale = new Vector3((length * player.myZaramins[player.activeZaramins[zaraminN]].getCurrentHealth())
                / player.myZaramins[player.activeZaramins[zaraminN]].getMaxHealth(),
                healthFill.transform.localScale.y, healthFill.transform.localScale.z);
            manaFill.transform.localScale = new Vector3((length * player.myZaramins[player.activeZaramins[zaraminN]].getCurrentMana())
                / player.myZaramins[player.activeZaramins[zaraminN]].getMaxMana(),
                manaFill.transform.localScale.y, manaFill.transform.localScale.z);
            checkFriendlyDeadAlpha(zaraminN);
        }
        else//Zaramin je levelov :D
        {
            healthFill = GameObject.Find("HostileZaramins/Zaramin" + (zaraminN+1) + "/HPMana/Health/TimeBarFill");
            manaFill = GameObject.Find("HostileZaramins/Zaramin" + (zaraminN+1) + "/HPMana/Mana/TimeBarFill");
            healthFill.transform.localScale = new Vector3((length * enemyZaramins[zaraminN-3].getCurrentHealth())
                / enemyZaramins[zaraminN-3].getMaxHealth(), healthFill.transform.localScale.y, healthFill.transform.localScale.z);
            manaFill.transform.localScale = new Vector3((length * enemyZaramins[zaraminN-3].getCurrentMana())
                / enemyZaramins[zaraminN-3].getMaxMana(), manaFill.transform.localScale.y, manaFill.transform.localScale.z);
            checkHostileDeadAlpha(zaraminN-3);
        }
    }
    
    private void drawDrawer()//Iscrtavanje magija
    {
        firstMagic.sprite = firstZaraminClicked.getMagics()[0].iconOn;

        if (firstZaraminClicked.getCooldowns()[1] < 1)//Da li su na cooldownu ili ne
            secondMagic.sprite = firstZaraminClicked.getMagics()[1].iconOn;
        else
            secondMagic.sprite = firstZaraminClicked.getMagics()[1].iconOff;

        if (firstZaraminClicked.getCooldowns()[2] < 1)
            thirdMagic.sprite = firstZaraminClicked.getMagics()[2].iconOn;
        else
            thirdMagic.sprite = firstZaraminClicked.getMagics()[2].iconOff;   
    }

    public string magicInfo(int n)
    {
        return firstZaraminClicked.getMagics()[n].getInfo();
    }

    private void Paint()//Iscrtavanje Zaramina koji su u borbi
    {
        int i;

        for (i = 1; i < 4; i++)
        {
            GameObject.Find("FriendlyZaramins/Zaramin" + i).GetComponent<SpriteRenderer>().sprite =
                player.myZaramins[player.activeZaramins[i - 1]].small;
        }

        for (i = 4; i < 7; i++)
            GameObject.Find("HostileZaramins/Zaramin" + i).GetComponent<SpriteRenderer>().sprite =
                enemyZaramins[(i - 1) % 3].small;
    }

    private void friendlyForward()
    {
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x, friendlyZaramins.transform.position.y, -2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x, hostileZaramins.transform.position.y, 2);
    }

    private void hostileForward()
    {
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x, friendlyZaramins.transform.position.y, 2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x, hostileZaramins.transform.position.y, -2);
    }

    private void prepareZaramins()
    {
        for (int i = 0; i < 3; i++)
        {
            if (player.myZaramins[player.activeZaramins[i]].getRace() == background.GetRace())
            {
                player.myZaramins[player.activeZaramins[i]].setMaxHealth();
            }

            if (enemyZaramins[i].getRace() == background.GetRace())
            {
                enemyZaramins[i].setMaxHealth();
            }
        }    
    }

    private bool checkPlayerWin()
    {
        return (enemyZaramins[0].isDead() && enemyZaramins[1].isDead() && enemyZaramins[2].isDead());
    }

    private bool checkComputerWin()
    {
        return (player.myZaramins[player.activeZaramins[0]].isDead() &&
                player.myZaramins[player.activeZaramins[1]].isDead() &&
                player.myZaramins[player.activeZaramins[2]].isDead());
    }

    public void winGame()
    {
        GameObject.Find("EndBox").transform.position = new Vector3 (0, 0, -8);
        GameObject.Find("EndBox/Win").GetComponent<TextMesh>().text = "YOU WON!";
        GameObject.Find("EndBox/WinText").GetComponent<TextMesh>().text = "Experience earned : 300\n\nGold earned: 100";
        Timer.StopTimer();
        if (player.currentLevel == player.getLevelReached())
            player.incLevelReached();
        player.incWins();
        player.incGold(100);
        endBattle(300, 300 / 3);
        script.BattleEnds();
    }

    public void loseGame()
    {
        GameObject.Find("EndBox").transform.position = new Vector3(0, 0, -8);
        GameObject.Find("EndBox/Win").GetComponent<TextMesh>().text = "YOU LOST!";
        GameObject.Find("EndBox/WinText").GetComponent<TextMesh>().text = "Experience earned : 150\n\nGold earned: 50";
        Timer.StopTimer();
        player.incGold(50);
        endBattle(150, 150 / 3);
        script.BattleEnds();
    }

    private bool onlyAliveStunned()
    {
        for (int i = 0; i < 3; i++)
        {
            if (enemyZaramins[i].isStunned())
                if (enemyZaramins[(i + 1) % 3].isDead() && enemyZaramins[(i + 2) % 3].isDead())
                    return true; 
        }
        return false;
    }

    private void endBattle(int playerExp, int zaraminExp)
    {
        player.changeExperience(playerExp);
        
        for (int i = 0; i < 3; i++)
        {
            player.myZaramins[player.activeZaramins[i]].changeExperience(zaraminExp);
            player.myZaramins[player.activeZaramins[i]].restoreAll();
        }
    }

    private void chooseBackground()
    {
        if (player.currentLevel == 1)
            background.setBackground("Earth");
        else
            if (player.currentLevel == 2)
                background.setBackground("Water");
            else
                if (player.currentLevel == 3)
                    background.setBackground("Dark");
                else
                    background.setBackground("Fire");
    }

    private void checkFriendlyDeadAlpha(int n)
    {
        if (player.myZaramins[player.activeZaramins[n]].isDead())
        {
            GameObject.Find("FriendlyZaramins/Zaramin" + (n + 1)).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
        }
    }
    private void checkHostileDeadAlpha(int n)
    {
        if (enemyZaramins[n].isDead())
        {
            GameObject.Find("HostileZaramins/Zaramin" + (n + 4)).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
        }
    }
}
