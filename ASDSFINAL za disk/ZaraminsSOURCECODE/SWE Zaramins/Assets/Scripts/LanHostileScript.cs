﻿using UnityEngine;
using System.Collections;

public class LanHostileScript : MonoBehaviour
{

    public LanGameControlScript lanGameController;
    private int number = 0;
    private TextMesh infoText;
    public SoundManagerScript script;

    // Use this for initialization
    void Start()
    {
        infoText = GameObject.Find("Info/InfoText").GetComponent<TextMesh>();
        lanGameController = GameObject.Find("GameController").GetComponent<LanGameControlScript>();
        script = GameObject.Find("SoundManager").GetComponent<SoundManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseUpAsButton()
    {
        if (lanGameController.phase == 2)
        {
            script.onClickSound();
            if (this.name == "Zaramin4")
                number = 0;
            else
                if (this.name == "Zaramin5")
                    number = 1;
                else
                    number = 2;
            lanGameController.phase23(number);
        }
        else
        {
            /*kod za info, mozda*/
        }
    }


    void OnMouseOver()
    {

        if (name == "Zaramin4")
        {
            infoText.text = lanGameController.enemyZaramins[0].getInfo();
        }
        else if (name == "Zaramin5")
        {
            infoText.text = lanGameController.enemyZaramins[1].getInfo();
        }
        else
        {
            infoText.text = lanGameController.enemyZaramins[2].getInfo();
        }

    }

    void OnMouseExit()
    {
        infoText.text = "";
    }
}
