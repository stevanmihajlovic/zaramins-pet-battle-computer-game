﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class GameControlScript : MonoBehaviour {
    public bool drawerVisible = false;
    public int phase = 0;
    public int level;

	public Zaramin firstZaraminClicked;
	public Zaramin secondZaraminClicked;
    public Zaramin[] enemyZaramins;

    public GameObject friendlyZaramins;//---->checked
    public GameObject hostileZaramins;//---->checked

     public GameObject playerTimerOpponent;

    public GameObject ChatInfo;

    public GameObject mask;//---->checked
    public GameObject drawer;//---->checked

	public PlayerScript player, opponent;//---->postavljaju se u prepare_battle metodi :D
	public Magic magicClicked;//---->menja vrednost tokom borbe
    public MagicPanelScript magicPanel;

    public SpriteRenderer firstMagic;
    public SpriteRenderer secondMagic;
    public SpriteRenderer thirdMagic;
    


	// Use this for initialization

	void Start ()
	{
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
        //opponent = GameObject.Find("Opponent").GetComponent<PlayerScript>();
        firstMagic = GameObject.Find("Drawer/FirstAttack").GetComponent<SpriteRenderer>();
        secondMagic = GameObject.Find("Drawer/SecondAttack").GetComponent<SpriteRenderer>();
        friendlyZaramins = GameObject.Find("FriendlyZaramins");
        hostileZaramins = GameObject.Find("HostileZaramins");
        mask = GameObject.Find("Mask");
        drawer = GameObject.Find("Drawer");
        magicPanel = GameObject.Find("MagicPanel").GetComponent<MagicPanelScript>();


        loadLevel(1);//----> ovo se dodaje samo radi testiranja! :D
      
        Debug.Log(player.getName());

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (phase == 3)
			phase = 0;	
	}

 
    public void phase01(Vector3 vector, int number)
    {
		firstZaraminClicked = player.myZaramins[player.activeZaramins[number]];
        drawerVisible = true;
        drawer.transform.position = vector;
        
        firstMagic.sprite = firstZaraminClicked.getMagics()[0].iconOff;
        secondMagic.sprite = firstZaraminClicked.getMagics()[1].iconOff;

        mask.transform.position = new Vector3(0, 0, 0);
        phase = 1;
        Debug.Log("0 ----> 1");
    }

    public void phase10()
    {
        drawer.transform.position = new Vector3(20, 0);
        drawerVisible = false;
        phase = 0;
        mask.transform.position = new Vector3(20, 0);
        Debug.Log("1 ----> 0");
    }
     
    public void phase12(int number)
    {
		//DODAJ ISPITIVANJE ZA MAGIJE!
        magicClicked = firstZaraminClicked.getMagics()[number];

        drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, 2);
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x,
                                                          friendlyZaramins.transform.position.y, 2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x,
                                                        hostileZaramins.transform.position.y, -2);
        phase = 2;
        Debug.Log("1 ----> 2");
        Debug.Log (magicClicked.getName());
    }

    public void phase21()
    {
		//PAMTI KOJA MAGIJA JE KLIKNUTA
        drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, -2);
        friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x,
		                                                  friendlyZaramins.transform.position.y, -2);
        hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x,
		                                                 hostileZaramins.transform.position.y, 2);
        phase = 1;
        Debug.Log("2 ----> 1");
    }

    public void phase23(int number)
    {
		friendlyZaramins.transform.position = new Vector3(friendlyZaramins.transform.position.x,
		                                                  friendlyZaramins.transform.position.y, -2);
		hostileZaramins.transform.position = new Vector3(hostileZaramins.transform.position.x,
		                                                 hostileZaramins.transform.position.y, 2);
        drawer.transform.position = new Vector3(20, drawer.transform.position.y, 2);
        mask.transform.position = new Vector3(20, 0);
        phase = 3;
        Debug.Log("2 ----> 3");
        Debug.Log("Napadnut zaramin!");
        do_attack(firstZaraminClicked, magicClicked, enemyZaramins[number]);
        Debug.Log(enemyZaramins[number].getCurrentHealth());
    }

    public void do_attack(Zaramin attacker,Magic m, Zaramin receiver)
    {
        
        attacker.attack(m, receiver);

    }
    public void loadLevel (int levelNo)
    {
        using (StreamReader s = new StreamReader(Application.dataPath + "/Levels/" + levelNo.ToString()+".txt"))
        {
            
            string type = s.ReadLine();
            if (type == "normal")
            {
                int count = Convert.ToInt32(s.ReadLine());
                enemyZaramins = new Zaramin[count];
                s.ReadLine();


                for (int i = 0; i<count; i++)
                {
                    string name = s.ReadLine();
                    string exp = s.ReadLine();
                    string typeZ = s.ReadLine();
                    string dht = s.ReadLine();
                    s.ReadLine();

                    enemyZaramins[i] = player.getZaramin(name,typeZ,dht,Convert.ToInt32(exp),magicPanel);
                }
            }
            else
            {

            }
        }
    }
    //public void update()
    //{
    //    for (int i = 0; i < 3; i++)
    //    {
    //        player.myZaramins[i].updateZaramin();
    //        opponent.myZaramins[i].updateZaramin();                     
    //    }
    //}
}
